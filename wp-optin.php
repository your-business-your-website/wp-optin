<?php
/*
Plugin Name: WP optin
Plugin URI: http://www.wpoptin.com/wpoptin
Description: Quickly and easily creates optin replay pages. No Branding.
Version: 1.0.6
Author: Armand Morin
Author URI: http://www.ArmandMorin.com/
*/

// *********************************************************************************** //
// UPDATE CHECKS
// *********************************************************************************** //
/**require 'plugin-updates/plugin-update-checker.php';
$ExampleUpdateChecker = new PluginUpdateChecker(
	'http://wp-optin.s3.amazonaws.com/wpoptin-updates.json',
	__FILE__,
	'wp-optin'
);

function getTinyUrl($url) {
	$tinyurl = file_get_contents("http://tinyurl.com/api-create.php?url=".$url);
	return $tinyurl;
}
 */

require_once 'inc/class-video-url-parser.php';
require_once 'mail/wp-delayed-mail.php';

// *********************************************************************************** //
// CREATE CUSTOM POST TYPE
// *********************************************************************************** //

add_action( 'init', 'wpoptin_init' );

function wpoptin_init() {
	$labels = array(
		'name'               => _x( 'WP optin', 'post type general name' ),
		'singular_name'      => _x( 'optin', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'optin' ),
		'add_new_item'       => __( 'Add optin Page' ),
		'edit_item'          => __( 'Edit optin Page' ),
		'new_item'           => __( 'New optin Page' ),
		'view_item'          => __( 'View optin Pages' ),
		'search_items'       => __( 'Search optin Pages' ),
		'not_found'          => __( 'No optins found' ),
		'not_found_in_trash' => __( 'No optins pages found in Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'WP optin',

	);
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'optin' ),
		'_builtin'           => false,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' ),
	);
	register_post_type( 'wpoptin', $args );
	flush_rewrite_rules();
}


// *********************************************************************************** //
// ADDS POST TYPE TO READING SETTINGS AND CORRECTS FRONT PAGE VIEW
// *********************************************************************************** //

add_filter( 'get_pages', 'add_my_optin' );
function add_my_optin( $pages ) {
	 $my_optin_pages = new WP_Query( array( 'post_type' => 'wpoptin' ) );
	if ( $my_optin_pages->post_count > 0 ) {
		$pages = array_merge( $pages, $my_optin_pages->posts );
	}
	 return $pages;
}

add_action( 'pre_get_posts', 'enable_front_page_optin' );
function enable_front_page_optin( $query ) {
	if ( '' == $query->query_vars['post_type'] && 0 != $query->query_vars['page_id'] ) {
		$query->query_vars['post_type'] = array( 'page', 'wpoptin' );
	}
}

add_action( 'template_redirect', 'template_redirect_frontpage_optin' );
function template_redirect_frontpage_optin() {
	if ( is_front_page() && is_singular( 'wpoptin' ) ) {
		include plugin_dir_path( __FILE__ ) . '/optin.php';
		die();
	}
}

add_action( 'template_redirect', 'optin_template_redirect' );
function optin_template_redirect() {
	global $wp_query;
	if ( $wp_query->query_vars['post_type'] == 'wpoptin' ) {
		include plugin_dir_path( __FILE__ ) . '/optin.php';
		die(); }
}


// *********************************************************************************** //
// ADDS PLUGIN MENU AND PAGE ICON
// *********************************************************************************** //

// add_action('admin_head', 'add_wpoptin_icon');
function add_wpoptin_icon() {
	global $post_type;
	?>
	<style>
	<?php if ( ( $_GET['post_type'] == 'wpoptin' ) || ( $post_type == 'wpoptin' ) ) : ?>
	#icon-edit { background:transparent url('<?php echo WP_PLUGIN_URL . '/wp-optin/images/wpoptin32.png'; ?>') no-repeat; }		
	<?php endif; ?>
	#adminmenu #menu-posts-wpoptin div.wp-menu-image{background:transparent url("<?php echo WP_PLUGIN_URL . '/wp-optin/images/wpoptin16.png'; ?>") no-repeat center center;}
	#adminmenu #menu-posts-wpoptin:hover div.wp-menu-image,#adminmenu #menu-posts-wpoptin.wp-has-current-submenu div.wp-menu-image{background:transparent url("<?php echo WP_PLUGIN_URL . '/wp-optin/images/wpoptin16.png'; ?>") no-repeat center center;}	
</style>
	<?php
}

// *********************************************************************************** //
// ADDS BACKWARD FUNCTIONALITY FOR SLL AND STANDARD VARIABLES
// *********************************************************************************** //

if ( ! function_exists( 'is_ssl' ) ) {
	function is_ssl() {
		if ( isset( $_SERVER['HTTPS'] ) ) {
			if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
				return true;
			}
			if ( '1' == $_SERVER['HTTPS'] ) {
				return true;
			}
		} elseif ( isset( $_SERVER['SERVER_PORT'] ) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
			return true;
		}
		return false;
	}
}

if ( version_compare( get_bloginfo( 'version' ), '3.0', '<' ) && is_ssl() ) {
	$wp_content_url = str_replace( 'http://', 'https://', get_option( 'siteurl' ) );
} else {
	$wp_content_url = get_option( 'siteurl' );
}
 $wp_content_url .= '/wp-content';
 $wp_content_dir  = ABSPATH . 'wp-content';
 $wp_plugin_url   = $wp_content_url . '/plugins';
 $wp_plugin_dir   = $wp_content_dir . '/plugins';
 $wpmu_plugin_url = $wp_content_url . '/mu-plugins';
 $wpmu_plugin_dir = $wp_content_dir . '/mu-plugins';

// *********************************************************************************** //
// ADDS TEMPLATE CHOICE
// *********************************************************************************** //

function wpoptin_template_redirect() {
	global $wp_query;
	if ( $wp_query->query_vars['post_type'] == 'wpoptin' ) {
		include plugin_dir_path( __FILE__ ) . '/optin.php';
		die(); }
}

add_action( 'template_redirect', 'wpoptin_template_redirect' );


// *********************************************************************************** //
// ADD STYLESHEET TO THEME PAGE
// *********************************************************************************** //

function wpoptin_enqueue_styles() {
	global $post_type;

	// if(isset($post_type) && $post_type == 'wpoptin') {
		wp_register_style( 'wpoptinstylesheet', WP_PLUGIN_URL . '/wp-optin/optin-style.css' );
		wp_enqueue_style( 'wpoptinstylesheet', plugins_url( '/wp-optin/optin-style.css', __FILE__ ), false, '1.1', 'all' );
	// }
}
add_action( 'wp_print_styles', 'wpoptin_enqueue_styles' );

function wpoptin_unregister_styles() {
	global $post_type;

	if ( isset( $post_type ) && $post_type != 'wpoptin' ) {
		wp_deregister_style( 'wpoptinstylesheet' );
	}
}
// add_action( 'wp_print_styles', 'wpoptin_unregister_styles' );

// Enqueue Javascript
function wpoptin_enqueue_js() {
	if ( ! is_admin() ) {

		wp_enqueue_script( 'le_script_supersize', WP_PLUGIN_URL . '/wp-optin/js/supersized.3.1.3.core.min.js', array( 'jquery' ), '1.0' );
		wp_enqueue_script( 'le_script_init', WP_PLUGIN_URL . '/wp-optin/js/init.js', array( 'jquery' ), '1.0' );
		wp_enqueue_script( 'miscjs', WP_PLUGIN_URL . '/wp-optin/js/misc.js' );
		wp_enqueue_script( 'countdown', WP_PLUGIN_URL . '/wp-optin/js/countdown.php' );
		wp_enqueue_script( 'adilo_player', 'https://cdn.bigcommand.com/dynamic-embed/js/inline.js', null, date( 'YmdHis' ), true );
		wp_enqueue_script( 'fastly_player', 'https://vsplayer.global.ssl.fastly.net/player-wrapper-v4.js', null, date( 'YmdHis' ), true );
	}}

// Actions
add_action( 'init', 'wpoptin_enqueue_js' );


// *********************************************************************************** //
// ADDS TINYMCE STYLESHEET
// *********************************************************************************** //

add_filter( 'mce_css', 'wpoptin_editor_style' );
function wpoptin_editor_style( $url ) {
	global $post_type;

	// if(isset($post_type) && $post_type == 'wpoptin') {

	if ( ! empty( $url ) ) {
		$url .= ',';
	}
		  // Change the path here if using sub-directory
		  $url .= WP_PLUGIN_URL . '/' . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . 'wpoptin-editor-style.css';

		  return $url;
	// }
}


// *********************************************************************************** //
// ADDS APPENDO FOR DUPLICATING RECURRING TIMES
// *********************************************************************************** //
/*
function add_admin_scripts( $hook ) {

	global $post;

	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		if( $post->post_type == 'wpoptin' ) {
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script(  'appendo', WP_PLUGIN_URL .'/wp-optin/js/jquery.appendo.js' );
			wp_enqueue_script(  'slider-manager', WP_PLUGIN_URL .'/wp-optin/js/manager.js' );
		}
	}
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );

*/
// *********************************************************************************** //
// ADMIN STYLES
// *********************************************************************************** //

function wpoptin_editor_styles() {
	global $post;
	global $post_type;
	if ( $post && $post->post_type && $post->post_type == 'wpoptin' ) {
		echo '
		<style type="text/css" media="screen">
			#content_code, #content_add_image, #content_add_video, #content_add_media, #content_add_audio { display: none !important; } 
			.wpoptin .description {font-style: normal; font-size: 12px; margin: 0px 0px 15px 0px;}
			.wpoptin #postcustomstuff table {border: none; background-color: #fff; padding: 4px; margin-bottom: 25px!important;}			 
			.wpoptin .shorttext {width: 50px !important; text-align: center;}			 
			.wpoptin input[type="text"] {height: 35px; width: 100%; margin-bottom: 15px;}
			.wp-color-picker {height: 24px !important; width: 60px !important; margin-bottom: 0px !important;}
			.wpoptin input[type="checkbox"] {margin-bottom: 15px;}
			.wpoptin label {font-weight: bold; font-size: 13px; margin-bottom: 8px; display: block;}
			.wpoptin #divid {width: 100%; margin: 0px auto 15px auto; border-bottom: 1px solid #ccc;}
			.wpoptin {padding: 10px;}
			.wpoptin #editorcontainer {margin-bottom: 15px;}
			.wpoptin .cbalign {width: 15px; height:15px; float: left;}
			.wpoptin .cblabel {padding-left: 3px; float: left; margin: 0px 15px 8px 0px; font-weight: normal; font-size: 12px;}
			.wpoptin #cbstuff {margin-right: 25px; float: left;}
			.wpoptin .mceResize {top: 0px !important; margin: 0px !important;}
			.wpoptin textarea {margin-bottom: 25px !important;}
			.wp-editor-container {background-color: #fff; margin-bottom: 25px;}
			.postbox {background-color: #fff;}
			#theme_image_checkbox input[type=checkbox] { display:none; }
			#theme_image_checkbox input[type=checkbox] + label {background-image: url(\'';

			echo get_template_directory_uri();
			echo '/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox input[type=checkbox]:checked + label {background-image: url(\'';
			echo get_template_directory_uri();
			echo '/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			
			#theme_image_checkbox1 input[type=checkbox] { display:none; }
			#theme_image_checkbox1 input[type=checkbox] + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-optin/templates/basic/images/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox1 input[type=checkbox]:checked + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-optin/templates/basic/images/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}


			#theme_image_checkbox2 input[type=checkbox] { display:none; }
			#theme_image_checkbox2 input[type=checkbox] + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-optin/templates/modern/images/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox2 input[type=checkbox]:checked + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-optin/templates/modern/images/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
		</style>
		';
	}
}
add_action( 'admin_head', 'wpoptin_editor_styles' );

// *********************************************************************************** //
// UPLOAD IMAGE JS FILE
// *********************************************************************************** //
/*
****

Needs to be wrapped in an if Statement



function pw_load_scripts($hook) {
	if( $hook != 'edit.php' && $hook != 'post.php' && $hook != 'post-new.php' )
		return;
	wp_enqueue_script( 'custom-js', plugins_url( 'wp-optin/js/custom-js.js' , dirname(__FILE__) ) );
}
add_action('admin_enqueue_scripts', 'pw_load_scripts');


********
*/

// *********************************************************************************** //
// ADDS JAVASCRIPT FOR AUTORESPONDER SELECTION
// *********************************************************************************** //

function wpoptin_arscript() {
	global $post;
	if ( $post && $post->post_type && $post->post_type == 'wpoptin' ) {
		?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#wpoptin_theme_color').wpColorPicker();
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {

	//	$('#getarcode').change(change_selects);
	
	$('#saveBTN').click(function() {
	
		$("#saveSettings").submit();
	
	  return false;
	});
	
	//	$('#wpoptin_optin_code').keyup(function() {
		$('#wpoptin_optin_code').bind('paste', function() {
		 setTimeout(function() {		
		//	alert("UP");
			
			 $fullARcode =  $('#wpoptin_optin_code').val(); 
			
			$.post("
			<?php
			echo WP_PLUGIN_URL;
			echo '/wp-optin/';
			?>
			archange.php", { code: ""+$fullARcode+"" },
			 function(data) {
			 $('#wpoptin_optin_code').val(data); 
				change_selects(); 
			 }); 
			 
	  return false; }, 100);
	});
	
	function change_selects(){
			var tags = ['a','iframe','frame','frameset','script'], reg, val = $('#wpoptin_optin_code').val(),
				hdn = $('#arcode_hdn_div2'), formurl = $('#wpoptin_optin_form_url'), hiddenfields = $('#wpoptin_optin_hidden_field');
			formurl.val('');
			if(jQuery.trim(val) == '')
				return false;
			$('#arcode_hdn_div').html('');
			$('#arcode_hdn_div2').html('');
			for(var i=0;i<5;i++){
				reg = new RegExp('<'+tags[i]+'([^<>+]*[^\/])>.*?</'+tags[i]+'>', "gi");
				val = val.replace(reg,'');
				
				reg = new RegExp('<'+tags[i]+'([^<>+]*)>', "gi");
				val = val.replace(reg,'');
			}
			var tmpval;
			try {
				tmpval = decodeURIComponent(val);
			} catch(err){
				tmpval = val;
			}
			hdn.append(tmpval);
			var num = 0;
			var name_selected = '';
			var email_selected = '';
			var phone_selected = '';
			$(':text',hdn).each(function(){
				var name = $(this).attr('name'),
					name_selected = num == '0' ? name : (num != '0' ? name_selected : ''), 
					email_selected = num == '1' ? name : email_selected;
					phone_selected = num == '2' ? name : phone_selected;
					if(num=='0') jQuery('#wpoptin_optin_name_field').val(name_selected);
					if(num=='1') jQuery('#wpoptin_optin_email_field').val(email_selected);
					if(num=='2') jQuery('#ar_phone').val(phone_selected);
			num++;
			});
			jQuery(':input[type=hidden]',hdn).each(function(){
				jQuery('#arcode_hdn_div').append(jQuery('<input type="hidden" name="'+jQuery(this).attr('name')+'" />').val(jQuery(this).val()));
			});		
			var hidden_f = jQuery('#arcode_hdn_div').html();
			formurl.val(jQuery('form',hdn).attr('action'));
			hiddenfields.val(hidden_f);
			hdn.html('');
			
		};


});
</script>

		<?php
	} }
add_action( 'admin_head', 'wpoptin_arscript' );

// Color Picker For Admin

function optin_Colorpicker() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker' );
}

add_action( 'admin_enqueue_scripts', 'optin_Colorpicker' );


// *********************************************************************************** //
// METABOXES
// *********************************************************************************** //

$prefix = 'wpoptin_';

$meta_boxes_optin = array(

	array(
		'id'       => 'optin_theme',
		'title'    => 'Theme Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => '',
				'desc' => 'Select the theme you wish to display for your optins. You can use your WordPress default them or you can utilize a choice of the themes built into WPoptin.',
				'id'   => $prefix . 'optinpage_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name' => 'Your Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_yours',
				'type' => 'checkboxtheme',
			),
			array(
				'name' => 'Basic Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_basic',
				'type' => 'checkboxtheme1',
				'std'  => '',
			),
			array(
				'name' => 'Modern Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_modern',
				'type' => 'checkboxtheme2',
				'std'  => '',
			),
			array(
				'name' => 'Theme Color',
				'desc' => 'Select the primary color of your theme. This function is only utilized with the built in WPoptin themes.',
				'id'   => $prefix . 'theme_color',
				'type' => 'colorpicker',
				'std'  => '#fff',
			),
			array(
				'name' => 'Upload Your Logo',
				'desc' => 'Upload the logo you would like to display on your webinas. This is only utilized with the built in WPoptin themes.',
				'id'   => $prefix . 'logo',
				'type' => 'image',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'settings_headline_mb',
		'title'    => 'optin Optin Page Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => '',
				'desc' => 'Some themes will require you to set the width of the optin page area as well as the top margin',
				'id'   => $prefix . 'optinpage_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name'    => 'Top Margin',
				'desc'    => 'Sets top margin of optin page.',
				'id'      => $prefix . 'top_margin',
				'type'    => 'shortselect',
				'options' => array( '', '5px', '10px', '15px', '20px', '25px', '30px', '35px', '40px', '45px', '50px', '55px', '60px', '65px', '70px', '75px', '80px', '85px', '90px', '95px', '100px' ),
			),
			array(
				'name'    => 'Sales Letter Width',
				'desc'    => 'Sets width of optin page.',
				'id'      => $prefix . 'wpoptin_width',
				'type'    => 'shortselect2',
				'options' => array( '', '800px', '810px', '820px', '830px', '840px', '850px', '860px', '870px', '880px', '890px', '900px', '910px', '920px', '930px', '940px', '950px', '960px', '970px', '980px', '990px', '1000px' ),
			),
		),
	),

	array(
		'id'       => 'main_headline_mb',
		'title'    => 'optin Optin Headline',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'optin Optin Headline',
				'desc' => 'Enter the main headline to appear at the top of your optin page. Use the Kitchen Sink to style it and toolbar to center.',
				'id'   => $prefix . 'main_headline',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'optin_media',
		'title'    => 'Media Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Media Settings',
				'desc' => 'Select to display a video or audio on your optin page. You video must be in an MP4 format to work properly.',
				'id'   => $prefix . 'media_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name' => 'Video',
				'desc' => '',
				'id'   => $prefix . 'show_video',
				'type' => 'checkboxvideo',
				'std'  => '',
			),
			array(
				'name' => 'Audio',
				'desc' => '',
				'id'   => $prefix . 'show_audio',
				'type' => 'checkboxaudio',
				'std'  => '',
			),
			array(
				'name' => 'Video URL',
				'desc' => 'Enter the location of the URL of your MP4 video below.',
				'id'   => $prefix . 'video_url',
				'type' => 'hiddenvideo1',
				'std'  => '',
			),
			array(
				'name' => 'Video Width',
				'desc' => 'Enter the width of your video. We recommend 600px wide or if you have a wide theme you may go up to 640px this can always change. Just enter a number, do not enter px after the number.',
				'id'   => $prefix . 'video_width',
				'type' => 'hiddenvideo2',
				'std'  => '',
			),
			array(
				'name' => 'Video Height',
				'desc' => 'Your video height will depend on where you did a 4x3 or 16x9 resolution of your video. Enter the number below.',
				'id'   => $prefix . 'video_height',
				'type' => 'hiddenvideo3',
				'std'  => '',
			),
			array(
				'name' => 'Audio MP3 URL',
				'desc' => 'Enter the URL of your MP3 file.',
				'id'   => $prefix . 'audio_url',
				'type' => 'hiddenaudio',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'optin_box_settings_mb',
		'title'    => 'Optin Box Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optin Box Headline',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'box_headline',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Optin Box Description',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'box_description',
				'type' => 'text',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'autoresponder_settings_mb',
		'title'    => 'Autoresponder Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Paste In Your Autoresponder Settings',
				'desc' => 'WPoptin works with any autoresponder system. Simply make sure your optin code has BOTH a name field and an email field. The name field must be first in the code. Most autoresponders work like this by default. Paste the code below. When successful you will see the 3 fields filled in with the correct field parameters.',
				'id'   => $prefix . 'optin_code',
				'type' => 'textarea',
			),
			array(
				'name' => 'Name Field',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_name_field',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Email Field',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_email_field',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Form URL',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_form_url',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Hidden Fields',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_hidden_field',
				'type' => 'hiddentextarea',
			),
		),
	),

	array(
		'id'       => 'optin_button_settings_mb',
		'title'    => 'Optin Button Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optin Button Text',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'button_text',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Choose Optin Buttons',
				'desc' => 'Select which button you would like to use.',
				'id'   => $prefix . 'button_orange',
				'type' => 'checkboxorange',
				'std'  => '',
			),
			array(
				'name' => '',
				'desc' => '',
				'id'   => $prefix . 'button_green',
				'type' => 'checkboxgreen',
				'std'  => '',
			),
			array(
				'name' => '',
				'desc' => '',
				'id'   => $prefix . 'button_blue',
				'type' => 'checkboxblue',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'optional_text_mb',
		'title'    => 'Optional Text Under Video',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optional Text Below Video',
				'desc' => 'This is an optional entry. If you choose, you can type text below the video with additional information. You may also insert images here through the media uploader.',
				'id'   => $prefix . 'optional_text',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'thankyou_page_text_mb',
		'title'    => 'Thank You Page Content',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Thank You Video URL',
				'desc' => 'Enter the URL for a video to be displayed on your thank you page.',
				'id'   => $prefix . 'thankyou_page_video_url',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Video Width',
				'desc' => 'Enter the width of the video below.',
				'id'   => $prefix . 'thankyou_page_video_width',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Video Height',
				'desc' => 'Enter the height of the video below.',
				'id'   => $prefix . 'thankyou_page_video_height',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Page Content',
				'desc' => 'Enter any additional information you want on the thank you page. This will appear under the video.',
				'id'   => $prefix . 'thankyou_page_content',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'optin_settings_mb',
		'title'    => 'optin Settings',
		'page'     => 'wpoptin',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Choose optin Timezone',
				'id'   => $prefix . 'optin_time_settings_text',
				'desc' => 'You can choose between local and admin time. Local time will always play the optin in the viewers local time zone. Admin time will force viewers to watch in your timezone. We highly advise you choose Local Time as it will garner the most people for your optins.',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'Local Time',
				'desc' => '',
				'id'   => $prefix . 'localtime',
				'type' => 'checkboxlocaltime',
				'std'  => '',
			),

			array(
				'name' => 'Admin Time',
				'desc' => '',
				'id'   => $prefix . 'admintime',
				'type' => 'checkboxadmintime',
				'std'  => '',
			),
			array(
				'name' => 'Select Your Timezone',
				'desc' => 'Choose your timezone from the dropdown menu below.',
				'id'   => $prefix . 'optin_timezone',
				'type' => 'timezoneselect',
				'std'  => '',
			),
			array(
				'name' => 'Select optin Occurance, Days and Times',
				'id'   => $prefix . 'optin_settings_text',
				'desc' => '',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'One Time optin',
				'desc' => '',
				'id'   => $prefix . 'onetime_optin',
				'type' => 'checkboxoptin',
				'std'  => '',
			),

			array(
				'name' => 'Recurring optin',
				'desc' => '',
				'id'   => $prefix . 'recurring_optin',
				'type' => 'checkboxoptins',
				'std'  => '',
			),
			array(
				'name' => 'One Time optin',
				'type' => 'onetimeoptintext',
				'id'   => $prefix . 'onetime_settings_text',
				'std'  => '',
			),
			array(
				'name' => 'optin Date:',
				'desc' => '',
				'id'   => $prefix . 'optin_month',
				'type' => 'monthselect',
				'std'  => '',
			),
			array(
				'name' => 'optin Date:',
				'desc' => '',
				'id'   => $prefix . 'optin_day',
				'type' => 'dayselect',
				'std'  => '',
			),
			array(
				'name' => 'optin Date:',
				'desc' => '',
				'id'   => $prefix . 'optin_year',
				'type' => 'yearselect',
				'std'  => '',
			),
			array(
				'name' => 'optin Time: ',
				'desc' => '',
				'id'   => $prefix . 'optin_hour',
				'type' => 'timeselect2',
				'std'  => '',
			),
			array(
				'name' => 'Choose Days of The Week',
				'desc' => 'Check which days of the week you want to play your optin.',
				'id'   => $prefix . 'dayofweek_text',
				'type' => 'plaintextrecurring',
				'std'  => '',
			),
			array(
				'name' => 'Sunday',
				'desc' => '',
				'id'   => $prefix . 'sunday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Monday',
				'desc' => '',
				'id'   => $prefix . 'monday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Tuesday',
				'desc' => '',
				'id'   => $prefix . 'tuesday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Wednesday',
				'desc' => '',
				'id'   => $prefix . 'wednesday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Thursday',
				'desc' => '',
				'id'   => $prefix . 'thursday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Friday',
				'desc' => '',
				'id'   => $prefix . 'friday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Saturday',
				'desc' => '',
				'id'   => $prefix . 'saturday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name'    => 'Recurring optin Time',
				'desc'    => '',
				'id'      => $prefix . 'recurring_time',
				'type'    => 'recurring_select_time',
				'std'     => '',
				'options' => array(
					'1'  => '01:00 AM',
					'2'  => '02:00 AM',
					'3'  => '03:00 AM',
					'4'  => '04:00 AM',
					'5'  => '05:00 AM',
					'6'  => '06:00 AM',
					'7'  => '07:00 AM',
					'8'  => '08:00 AM',
					'9'  => '09:00 AM',
					'10' => '10:00 AM',
					'11' => '11:00 AM',
					'12' => '12:00 PM',
					'13' => '01:00 PM',
					'14' => '02:00 PM',
					'15' => '03:00 PM',
					'16' => '04:00 PM',
					'17' => '05:00 PM',
					'18' => '06:00 PM',
					'19' => '07:00 PM',
					'20' => '08:00 PM',
					'21' => '09:00 PM',
					'22' => '10:00 PM',
					'23' => '11:00 PM',
					'24' => '12:00 AM',
				),

			),
			array(
				'name' => 'optin Video URL',
				'desc' => 'Enter the URL for your recorded optin video.',
				'id'   => $prefix . 'optin_video_url',
				'type' => 'optinvideotext',
				'std'  => '',
			),
			array(
				'name' => 'optin Video Width',
				'desc' => 'Enter the width of your optin video.',
				'id'   => $prefix . 'optin_video_width',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'optin Video Height',
				'desc' => 'Enter the height of your optin video.',
				'id'   => $prefix . 'optin_video_height',
				'type' => 'text',
				'std'  => '',
			),
		),
	),
);


// *********************************************************************************** //
// REGISTERS THE METABOX
// *********************************************************************************** //

add_action( 'add_meta_boxes', 'wpoptin_add_box' );

function wpoptin_add_box() {
	global $meta_boxes_optin;

	foreach ( $meta_boxes_optin as $meta_box_optin ) :
		add_meta_box( $meta_box_optin['id'], $meta_box_optin['title'], 'wpoptin_show_box', $meta_box_optin['page'], $meta_box_optin['context'], $meta_box_optin['priority'], array( 'id' => $meta_box_optin['id'] ) );
	endforeach;
}

// *********************************************************************************** //
// METABOX CALLBACK AND DISPLAY
// *********************************************************************************** //

function wpoptin_show_box( $post, $mb_id ) {
	global $meta_boxes_optin, $post, $post_id;
	wp_nonce_field( plugin_basename( __FILE__ ), 'wpoptin_meta_box_nonce' );
	wp_create_nonce( 'wpoptin_meta_box_nonce' );

	// Use nonce for verification
	echo '<input type="hidden" id="wpoptin_meta_box_nonce" name="wpoptin_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />' . "\n";

	echo '<div class="wpoptin">' . "\n";

	foreach ( $meta_boxes_optin as $meta_box_optin ) {
		if ( $meta_box_optin['id'] == $mb_id['id'] ) :
			foreach ( $meta_box_optin['fields'] as $field ) {
				// get current post meta data

				$meta = get_post_meta( $post->ID, $field['id'], true );

				switch ( $field['type'] ) {
					case 'image':
						$image = WP_PLUGIN_URL . '/wp-optin-full/images/default.png';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<span class="custom_default_image" style="display:none">' . $image . '</span>';
						if ( $meta ) {
							$image = wp_get_attachment_image_src( $meta, 'medium' );
							$image = $image[0]; }
						echo '<input name="' . $field['id'] . '" type="hidden" class="custom_upload_image" value="' . $meta . '" />
									<img src="' . $image . '" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small><a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" />';
						break;
					case 'text':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" />' . "\n";
						break;

					case 'colorpicker':
						echo '<div id="image_uploader" style="clear: both; margin-bottom: 15px;"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" data-default-color="#ffffff" /></div>' . "\n";
						break;

					case 'texthidden':
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:100%">' . "\n";
						break;

					case 'textarea':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="10" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						break;

					case 'hiddentextarea':
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:100%; display:none">', $meta ? $meta : $field['std'], '</textarea><div id="arcode_hdn_div" style="display: none;"></div><div id="arcode_hdn_div2" style="display: none;"></div>';
						break;

					case 'minutes':
						echo '<input type="text" class="shorttext" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" /><strong> : </strong>';
						break;

					case 'seconds':
						echo '<input type="text" class="shorttext" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" />' . "\n";
						break;

					case 'optinvideotext':
						echo '</div><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" />' . "\n";
						break;

					case 'onetimeoptintext':
						echo '<div id="onetime_box" style="display:none; border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;"><label for="', $field['id'], '">', $field['name'], '</label>' . "\n";
						break;

					case 'testtext':
						echo '<table class="appendo"><tr><td><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" /></tr></td></table>' . "\n";
						break;

					case 'plaintext':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						break;

					case 'plaintextrecurring':
						echo '<div id="recurring_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						break;

					// HIDDEN VIDEO AUDIO CASE

					case 'hiddenvideo1':
						echo '<div id="video_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						break;

					case 'hiddenvideo2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						break;

					case 'hiddenvideo3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>';
						break;

					case 'hiddenaudio':
						echo '<div id="audio_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">' . "\n";
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>' . "\n";
						break;

					// HIDDEN AWEBER

					case 'hiddenaweber1':
						echo '<div id="aweber_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">' . "\n";
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>';
						break;

					case 'hiddenaweber2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN 1SHOPPINGCART

					case 'hidden1shoppingcart1':
						echo '<div id="1shoppingcart_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hidden1shoppingcart2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hidden1shoppingcart3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN GETRESPONSE

					case 'hiddengetresponse1':
						echo '<div id="getresponse_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					case 'hiddengetresponse2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddengetresponse3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN ARP3

					case 'hiddenarp31':
						echo '<div id="arp3_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddenarp32':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddenarp33':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN OTHER

					case 'hiddenother':
						echo '<div id="other_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						echo '</div>';
						break;

					case 'textarea':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="8" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						break;

					case 'wysiwyg':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						$settings = array( 'textarea_rows' => '3' );
						wp_editor( $meta ? $meta : $field['std'], $field['id'], $settings );
						break;

					case 'select':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						break;

					case 'selecttestoriginal':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $value => $option ) {
							echo '<option value="',$value,'"', $meta == $value ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						break;

					// TIME SELECT TEST2 $meta = get_post_meta($post->ID, $field['id'], true);
					case 'recurring_select_time':
						global $post;
						$post_id = $post->ID;

						echo '<div style="clear:both;"><label for="', $field['id'], '">', $field['name'], '</label></div><div class="description">', $field['desc'],'</div>' . "\n";

						echo '<ul id="tester_wrap">' . "\n";

						if ( get_post_meta( $post_id, 'wpoptin_recurring_time', true ) ) :

									$a = get_post_meta( $post->ID, 'wpoptin_recurring_time', true );
							foreach ( $a as $k => $v ) {
								echo '<li class="tester">' . "\n";

								echo '<select name="wpoptin_recurring_time[]">' . "\n";
								foreach ( $field['options'] as $value => $option ) {
									echo '<option	value= "' . $value . '" ' . ( ( $v == $value ) ? 'selected="selected"' : '' ) . '>	' . $option . '</option>	' . $v . '' . "\n";
								}
								echo '</select><button class="remove_slide button-secondary">Remove</button><div style="clear:both"></div>' . "\n";

								echo '</li>' . "\n";
							}

							else :

								echo '<li class="tester">';

									echo '<select name="wpoptin_recurring_time[]">';
								foreach ( $field['options'] as $value => $option ) {
									echo '<option value="',$value,'">', $option, '</option>';
								}
									echo '</select>';

								echo '</li>';

							endif;

							echo '</ul>' . "\n";
						break;

					// TIME SELECT TEST CURRENT

					case 'timeselect':
						echo '<div style="clear: both"></div><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						global $post;
						$post_id = $post->ID;

						$optintime = get_post_meta( $post_id, 'wpoptin_optin_time', true );

						echo '<table class="appendo"><tr><td><select name="wpoptin_optin_time[]" id="wpoptin_optin_time[]">';
						echo '<option value="">Select...</option>' . "\n";
						echo '<option value="1"' . ( ( $optintime == '1' ) ? 'selected="selected"' : '' ) . '>01:00 AM</option>' . "\n";
						echo '<option value="2"' . ( ( $optintime == '2' ) ? 'selected="selected"' : '' ) . '>02:00 AM</option>' . "\n";
						echo '<option value="3"' . ( ( $optintime == '3' ) ? 'selected="selected"' : '' ) . '>03:00 AM</option>' . "\n";
						echo '<option value="4"' . ( ( $optintime == '4' ) ? 'selected="selected"' : '' ) . '>04:00 AM</option>' . "\n";
						echo '<option value="5"' . ( ( $optintime == '5' ) ? 'selected="selected"' : '' ) . '>05:00 AM</option>' . "\n";
						echo '<option value="6"' . ( ( $optintime == '6' ) ? 'selected="selected"' : '' ) . '>06:00 AM</option>' . "\n";
						echo '<option value="7"' . ( ( $optintime == '7' ) ? 'selected="selected"' : '' ) . '>07:00 AM</option>' . "\n";
						echo '<option value="8"' . ( ( $optintime == '8' ) ? 'selected="selected"' : '' ) . '>08:00 AM</option>' . "\n";
						echo '<option value="9"' . ( ( $optintime == '9' ) ? 'selected="selected"' : '' ) . '>09:00 AM</option>' . "\n";
						echo '<option value="10"' . ( ( $optintime == '10' ) ? 'selected="selected"' : '' ) . '>10:00 AM</option>' . "\n";
						echo '<option value="11"' . ( ( $optintime == '11' ) ? 'selected="selected"' : '' ) . '>11:00 AM</option>' . "\n";
						echo '<option value="12"' . ( ( $optintime == '12' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '<option value="13"' . ( ( $optintime == '13' ) ? 'selected="selected"' : '' ) . '>01:00 PM</option>' . "\n";
						echo '<option value="14"' . ( ( $optintime == '14' ) ? 'selected="selected"' : '' ) . '>02:00 PM</option>' . "\n";
						echo '<option value="15"' . ( ( $optintime == '15' ) ? 'selected="selected"' : '' ) . '>03:00 PM</option>' . "\n";
						echo '<option value="16"' . ( ( $optintime == '16' ) ? 'selected="selected"' : '' ) . '>04:00 PM</option>' . "\n";
						echo '<option value="17"' . ( ( $optintime == '17' ) ? 'selected="selected"' : '' ) . '>05:00 PM</option>' . "\n";
						echo '<option value="18"' . ( ( $optintime == '18' ) ? 'selected="selected"' : '' ) . '>06:00 PM</option>' . "\n";
						echo '<option value="19"' . ( ( $optintime == '19' ) ? 'selected="selected"' : '' ) . '>07:00 PM</option>' . "\n";
						echo '<option value="20"' . ( ( $optintime == '20' ) ? 'selected="selected"' : '' ) . '>08:00 PM</option>' . "\n";
						echo '<option value="21"' . ( ( $optintime == '21' ) ? 'selected="selected"' : '' ) . '>09:00 PM</option>' . "\n";
						echo '<option value="22"' . ( ( $optintime == '22' ) ? 'selected="selected"' : '' ) . '>10:00 PM</option>' . "\n";
						echo '<option value="23"' . ( ( $optintime == '23' ) ? 'selected="selected"' : '' ) . '>11:00 PM</option>' . "\n";
						echo '<option value="24"' . ( ( $optintime == '24' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '</select></td></tr></table>';
						break;

					case 'timeselect2':
						echo $field['name'];
						global $post;
						$post_id    = $post->ID;
						$optintime2 = get_post_meta( $post_id, 'wpoptin_optin_hour', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="">Select...</option>' . "\n";
						echo '<option value="1"' . ( ( $optintime2 == '1' ) ? 'selected="selected"' : '' ) . '>01:00 AM</option>' . "\n";
						echo '<option value="2"' . ( ( $optintime2 == '2' ) ? 'selected="selected"' : '' ) . '>02:00 AM</option>' . "\n";
						echo '<option value="3"' . ( ( $optintime2 == '3' ) ? 'selected="selected"' : '' ) . '>03:00 AM</option>' . "\n";
						echo '<option value="4"' . ( ( $optintime2 == '4' ) ? 'selected="selected"' : '' ) . '>04:00 AM</option>' . "\n";
						echo '<option value="5"' . ( ( $optintime2 == '5' ) ? 'selected="selected"' : '' ) . '>05:00 AM</option>' . "\n";
						echo '<option value="6"' . ( ( $optintime2 == '6' ) ? 'selected="selected"' : '' ) . '>06:00 AM</option>' . "\n";
						echo '<option value="7"' . ( ( $optintime2 == '7' ) ? 'selected="selected"' : '' ) . '>07:00 AM</option>' . "\n";
						echo '<option value="8"' . ( ( $optintime2 == '8' ) ? 'selected="selected"' : '' ) . '>08:00 AM</option>' . "\n";
						echo '<option value="9"' . ( ( $optintime2 == '9' ) ? 'selected="selected"' : '' ) . '>09:00 AM</option>' . "\n";
						echo '<option value="10"' . ( ( $optintime2 == '10' ) ? 'selected="selected"' : '' ) . '>10:00 AM</option>' . "\n";
						echo '<option value="11"' . ( ( $optintime2 == '11' ) ? 'selected="selected"' : '' ) . '>11:00 AM</option>' . "\n";
						echo '<option value="12"' . ( ( $optintime2 == '12' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '<option value="13"' . ( ( $optintime2 == '13' ) ? 'selected="selected"' : '' ) . '>01:00 PM</option>' . "\n";
						echo '<option value="14"' . ( ( $optintime2 == '14' ) ? 'selected="selected"' : '' ) . '>02:00 PM</option>' . "\n";
						echo '<option value="15"' . ( ( $optintime2 == '15' ) ? 'selected="selected"' : '' ) . '>03:00 PM</option>' . "\n";
						echo '<option value="16"' . ( ( $optintime2 == '16' ) ? 'selected="selected"' : '' ) . '>04:00 PM</option>' . "\n";
						echo '<option value="17"' . ( ( $optintime2 == '17' ) ? 'selected="selected"' : '' ) . '>05:00 PM</option>' . "\n";
						echo '<option value="18"' . ( ( $optintime2 == '18' ) ? 'selected="selected"' : '' ) . '>06:00 PM</option>' . "\n";
						echo '<option value="19"' . ( ( $optintime2 == '19' ) ? 'selected="selected"' : '' ) . '>07:00 PM</option>' . "\n";
						echo '<option value="20"' . ( ( $optintime2 == '20' ) ? 'selected="selected"' : '' ) . '>08:00 PM</option>' . "\n";
						echo '<option value="21"' . ( ( $optintime2 == '21' ) ? 'selected="selected"' : '' ) . '>09:00 PM</option>' . "\n";
						echo '<option value="22"' . ( ( $optintime2 == '22' ) ? 'selected="selected"' : '' ) . '>10:00 PM</option>' . "\n";
						echo '<option value="23"' . ( ( $optintime2 == '23' ) ? 'selected="selected"' : '' ) . '>11:00 PM</option>' . "\n";
						echo '<option value="24"' . ( ( $optintime2 == '24' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '</select>';
						echo '</div>';
						break;

					case 'monthselect':
						echo $field['name'];
						global $post;
						$post_id    = $post->ID;
						$optinmonth = get_post_meta( $post_id, 'wpoptin_optin_month', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="1"' . ( ( $optinmonth == '1' ) ? 'selected="selected"' : '' ) . '>January</option>' . "\n";
						echo '<option value="2"' . ( ( $optinmonth == '2' ) ? 'selected="selected"' : '' ) . '>February</option>' . "\n";
						echo '<option value="3"' . ( ( $optinmonth == '3' ) ? 'selected="selected"' : '' ) . '>March</option>' . "\n";
						echo '<option value="4"' . ( ( $optinmonth == '4' ) ? 'selected="selected"' : '' ) . '>April</option>' . "\n";
						echo '<option value="5"' . ( ( $optinmonth == '5' ) ? 'selected="selected"' : '' ) . '>May</option>' . "\n";
						echo '<option value="6"' . ( ( $optinmonth == '6' ) ? 'selected="selected"' : '' ) . '>June</option>' . "\n";
						echo '<option value="7"' . ( ( $optinmonth == '7' ) ? 'selected="selected"' : '' ) . '>July</option>' . "\n";
						echo '<option value="8"' . ( ( $optinmonth == '8' ) ? 'selected="selected"' : '' ) . '>August</option>' . "\n";
						echo '<option value="9"' . ( ( $optinmonth == '9' ) ? 'selected="selected"' : '' ) . '>September</option>' . "\n";
						echo '<option value="10"' . ( ( $optinmonth == '10' ) ? 'selected="selected"' : '' ) . '>October</option>' . "\n";
						echo '<option value="11"' . ( ( $optinmonth == '11' ) ? 'selected="selected"' : '' ) . '>November</option>' . "\n";
						echo '<option value="12"' . ( ( $optinmonth == '12' ) ? 'selected="selected"' : '' ) . '>December</option>' . "\n";
						echo '</select>';
						break;

					case 'dayselect':
						global $post;
						$post_id  = $post->ID;
						$optinday = get_post_meta( $post_id, 'wpoptin_optin_day', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option  value="1"' . ( ( $optinday == '1' ) ? 'selected="selected"' : '' ) . '>1</option>' . "\n";
						echo '<option  value="2"' . ( ( $optinday == '2' ) ? 'selected="selected"' : '' ) . '>2</option>' . "\n";
						echo '<option  value="3"' . ( ( $optinday == '3' ) ? 'selected="selected"' : '' ) . '>3</option>' . "\n";
						echo '<option  value="4"' . ( ( $optinday == '4' ) ? 'selected="selected"' : '' ) . '>4</option>' . "\n";
						echo '<option  value="5"' . ( ( $optinday == '5' ) ? 'selected="selected"' : '' ) . '>5</option>' . "\n";
						echo '<option  value="6"' . ( ( $optinday == '6' ) ? 'selected="selected"' : '' ) . '>6</option>' . "\n";
						echo '<option  value="7"' . ( ( $optinday == '7' ) ? 'selected="selected"' : '' ) . '>7</option>' . "\n";
						echo '<option  value="8"' . ( ( $optinday == '8' ) ? 'selected="selected"' : '' ) . '>8</option>' . "\n";
						echo '<option  value="9"' . ( ( $optinday == '9' ) ? 'selected="selected"' : '' ) . '>9</option>' . "\n";
						echo '<option  value="10"' . ( ( $optinday == '10' ) ? 'selected="selected"' : '' ) . '>10</option>' . "\n";
						echo '<option  value="11"' . ( ( $optinday == '11' ) ? 'selected="selected"' : '' ) . '>11</option>' . "\n";
						echo '<option  value="12"' . ( ( $optinday == '12' ) ? 'selected="selected"' : '' ) . '>12</option>' . "\n";
						echo '<option  value="13"' . ( ( $optinday == '13' ) ? 'selected="selected"' : '' ) . '>13</option>' . "\n";
						echo '<option  value="14"' . ( ( $optinday == '14' ) ? 'selected="selected"' : '' ) . '>14</option>' . "\n";
						echo '<option  value="15"' . ( ( $optinday == '15' ) ? 'selected="selected"' : '' ) . '>15</option>' . "\n";
						echo '<option  value="16"' . ( ( $optinday == '16' ) ? 'selected="selected"' : '' ) . '>16</option>' . "\n";
						echo '<option  value="17"' . ( ( $optinday == '17' ) ? 'selected="selected"' : '' ) . '>17</option>' . "\n";
						echo '<option  value="18"' . ( ( $optinday == '18' ) ? 'selected="selected"' : '' ) . '>18</option>' . "\n";
						echo '<option  value="19"' . ( ( $optinday == '19' ) ? 'selected="selected"' : '' ) . '>19</option>' . "\n";
						echo '<option  value="20"' . ( ( $optinday == '20' ) ? 'selected="selected"' : '' ) . '>20</option>' . "\n";
						echo '<option  value="21"' . ( ( $optinday == '21' ) ? 'selected="selected"' : '' ) . '>21</option>' . "\n";
						echo '<option  value="22"' . ( ( $optinday == '22' ) ? 'selected="selected"' : '' ) . '>22</option>' . "\n";
						echo '<option  value="23"' . ( ( $optinday == '23' ) ? 'selected="selected"' : '' ) . '>23</option>' . "\n";
						echo '<option  value="24"' . ( ( $optinday == '24' ) ? 'selected="selected"' : '' ) . '>24</option>' . "\n";
						echo '<option  value="25"' . ( ( $optinday == '25' ) ? 'selected="selected"' : '' ) . '>25</option>' . "\n";
						echo '<option  value="26"' . ( ( $optinday == '26' ) ? 'selected="selected"' : '' ) . '>26</option>' . "\n";
						echo '<option  value="27"' . ( ( $optinday == '27' ) ? 'selected="selected"' : '' ) . '>27</option>' . "\n";
						echo '<option  value="28"' . ( ( $optinday == '28' ) ? 'selected="selected"' : '' ) . '>28</option>' . "\n";
						echo '<option  value="29"' . ( ( $optinday == '29' ) ? 'selected="selected"' : '' ) . '>29</option>' . "\n";
						echo '<option  value="30"' . ( ( $optinday == '30' ) ? 'selected="selected"' : '' ) . '>30</option>' . "\n";
						echo '<option  value="31"' . ( ( $optinday == '31' ) ? 'selected="selected"' : '' ) . '>31</option>' . "\n";
						echo '</select>';
						break;

					case 'yearselect':
						global $post;
						$post_id   = $post->ID;
						$optinyear = get_post_meta( $post_id, 'wpoptin_optin_year', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option  value="2011"' . ( ( $optinyear == '2011' ) ? 'selected="selected"' : '' ) . '>2011</option>' . "\n";
						echo '<option  value="2012"' . ( ( $optinyear == '2012' ) ? 'selected="selected"' : '' ) . '>2012</option>' . "\n";
						echo '<option  value="2013"' . ( ( $optinyear == '2013' ) ? 'selected="selected"' : '' ) . '>2013</option>' . "\n";
						echo '<option  value="2014"' . ( ( $optinyear == '2014' ) ? 'selected="selected"' : '' ) . '>2014</option>' . "\n";
						echo '<option  value="2015"' . ( ( $optinyear == '2015' ) ? 'selected="selected"' : '' ) . '>2015</option>' . "\n";
						echo '<option  value="2016"' . ( ( $optinyear == '2016' ) ? 'selected="selected"' : '' ) . '>2016</option>' . "\n";
						echo '<option  value="2017"' . ( ( $optinyear == '2017' ) ? 'selected="selected"' : '' ) . '>2017</option>' . "\n";
						echo '<option  value="2018"' . ( ( $optinyear == '2018' ) ? 'selected="selected"' : '' ) . '>2018</option>' . "\n";
						echo '<option  value="2019"' . ( ( $optinyear == '2019' ) ? 'selected="selected"' : '' ) . '>2019</option>' . "\n";
						echo '<option  value="2020"' . ( ( $optinyear == '2020' ) ? 'selected="selected"' : '' ) . '>2020</option>' . "\n";
						echo '<option  value="2021"' . ( ( $optinyear == '2021' ) ? 'selected="selected"' : '' ) . '>2021</option>' . "\n";
						echo '</select>';
						break;

					case 'timezoneselect':
						global $post;
						$post_id       = $post->ID;
						$optintimezone = get_post_meta( $post_id, 'wpoptin_optin_timezone', true );
						echo '<div id ="admintimebox" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="Pacific/Midway"' . ( ( $optintimezone == 'Pacific/Midway' ) ? 'selected="selected"' : '' ) . '>(GMT-11:00) Midway Island, Samoa</option>' . "\n";
						echo '<option value="America/Adak"' . ( ( $optintimezone == 'America/Adak' ) ? 'selected="selected"' : '' ) . '>(GMT-10:00) Hawaii-Aleutian</option>' . "\n";
						echo '<option value="Etc/GMT+10"' . ( ( $optintimezone == 'Etc/GMT+10' ) ? 'selected="selected"' : '' ) . '>(GMT-10:00) Hawaii</option>' . "\n";
						echo '<option value="Pacific/Marquesas"' . ( ( $optintimezone == 'Pacific/Marquesas' ) ? 'selected="selected"' : '' ) . '>(GMT-09:30) Marquesas Islands</option>' . "\n";
						echo '<option value="Pacific/Gambier"' . ( ( $optintimezone == 'Pacific/Gambier' ) ? 'selected="selected"' : '' ) . '>(GMT-09:00) Gambier Islands</option>' . "\n";
						echo '<option value="America/Anchorage"' . ( ( $optintimezone == 'America/Anchorage' ) ? 'selected="selected"' : '' ) . '>(GMT-09:00) Alaska</option>' . "\n";
						echo '<option value="America/Ensenada"' . ( ( $optintimezone == 'America/Ensenada' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Tijuana, Baja California</option>' . "\n";
						echo '<option value="Etc/GMT+8"' . ( ( $optintimezone == 'Etc/GMT+8' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Pitcairn Islands</option>' . "\n";
						echo '<option value="America/Los_Angeles"' . ( ( $optintimezone == 'America/Los_Angeles' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Pacific Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Denver"' . ( ( $optintimezone == 'America/Denver' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Mountain Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Chihuahua"' . ( ( $optintimezone == 'America/Chihuahua' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>' . "\n";
						echo '<option value="America/Dawson_Creek"' . ( ( $optintimezone == 'America/Dawson_Creek' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Arizona</option>' . "\n";
						echo '<option value="America/Belize"' . ( ( $optintimezone == 'America/Belize' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Saskatchewan, Central America</option>' . "\n";
						echo '<option value="America/Cancun"' . ( ( $optintimezone == 'America/Cancun' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>' . "\n";
						echo '<option value="Chile/EasterIsland"' . ( ( $optintimezone == 'Chile/EasterIsland' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Easter Island</option>' . "\n";
						echo '<option value="America/Chicago"' . ( ( $optintimezone == 'America/Chicago' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Central Time (US & Canada)</option>' . "\n";
						echo '<option value="America/New_York"' . ( ( $optintimezone == 'America/New_York' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Eastern Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Havana"' . ( ( $optintimezone == 'America/Havana' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Cuba</option>' . "\n";
						echo '<option value="America/Bogota"' . ( ( $optintimezone == 'America/Bogota' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>' . "\n";
						echo '<option value="America/Caracas"' . ( ( $optintimezone == 'America/Caracas' ) ? 'selected="selected"' : '' ) . '>(GMT-04:30) Caracas</option>' . "\n";
						echo '<option value="America/Santiago"' . ( ( $optintimezone == 'America/Santiago' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Santiago</option>' . "\n";
						echo '<option value="America/La_Paz"' . ( ( $optintimezone == 'America/La_Paz' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) La Paz</option>' . "\n";
						echo '<option value="Atlantic/Stanley"' . ( ( $optintimezone == 'Atlantic/Stanley' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Faukland Islands</option>' . "\n";
						echo '<option value="America/Campo_Grande"' . ( ( $optintimezone == 'America/Campo_Grande' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Brazil</option>' . "\n";
						echo '<option value="America/Goose_Bay"' . ( ( $optintimezone == 'America/Goose_Bay' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Atlantic Time (Goose Bay)</option>' . "\n";
						echo '<option value="America/Glace_Bay"' . ( ( $optintimezone == 'America/Glace_Bay' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Atlantic Time (Canada)</option>' . "\n";
						echo '<option value="America/St_Johns"' . ( ( $optintimezone == 'America/St_Johns' ) ? 'selected="selected"' : '' ) . '>(GMT-03:30) Newfoundland</option>' . "\n";
						echo '<option value="America/Araguaina"' . ( ( $optintimezone == 'America/Araguaina' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) UTC-3</option>' . "\n";
						echo '<option value="America/Montevideo"' . ( ( $optintimezone == 'America/Montevideo' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Montevideo</option>' . "\n";
						echo '<option value="America/Miquelon"' . ( ( $optintimezone == 'America/Miquelon' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Miquelon, St. Pierre</option>' . "\n";
						echo '<option value="America/Godthab"' . ( ( $optintimezone == 'America/Godthab' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Greenland</option>' . "\n";
						echo '<option value="America/Argentina/Buenos_Aires"' . ( ( $optintimezone == 'America/Argentina/Buenos_Aires' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Buenos Aires</option>' . "\n";
						echo '<option value="America/Sao_Paulo"' . ( ( $optintimezone == 'America/Sao_Paulo' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Brasilia</option>' . "\n";
						echo '<option value="America/Noronha"' . ( ( $optintimezone == 'America/Noronha' ) ? 'selected="selected"' : '' ) . '>(GMT-02:00) Mid-Atlantic</option>' . "\n";
						echo '<option value="Atlantic/Cape_Verde"' . ( ( $optintimezone == 'Atlantic/Cape_Verde' ) ? 'selected="selected"' : '' ) . '>(GMT-01:00) Cape Verde Is.</option>' . "\n";
						echo '<option value="Atlantic/Azores"' . ( ( $optintimezone == 'Atlantic/Azores' ) ? 'selected="selected"' : '' ) . '>(GMT-01:00) Azores</option>' . "\n";
						echo '<option value="Europe/Belfast"' . ( ( $optintimezone == 'Europe/Belfast' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Belfast</option>' . "\n";
						echo '<option value="Europe/Dublin"' . ( ( $optintimezone == 'Europe/Dublin' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Dublin</option>' . "\n";
						echo '<option value="Europe/Lisbon"' . ( ( $optintimezone == 'Europe/Lisbon' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Lisbon</option>' . "\n";
						echo '<option value="Europe/London"' . ( ( $optintimezone == 'Europe/London' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : London</option>' . "\n";
						echo '<option value="Africa/Abidjan"' . ( ( $optintimezone == 'Africa/Abidjan' ) ? 'selected="selected"' : '' ) . '>(GMT) Monrovia, Reykjavik</option>' . "\n";
						echo '<option value="Europe/Amsterdam"' . ( ( $optintimezone == 'Europe/Amsterdam' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>' . "\n";
						echo '<option value="Europe/Belgrade"' . ( ( $optintimezone == 'Europe/Belgrade' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>' . "\n";
						echo '<option value="Europe/Brussels"' . ( ( $optintimezone == 'Europe/Brussels' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>' . "\n";
						echo '<option value="Africa/Algiers"' . ( ( $optintimezone == 'Africa/Algiers' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) West Central Africa</option>' . "\n";
						echo '<option value="Africa/Windhoek"' . ( ( $optintimezone == 'Africa/Windhoek' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Windhoek</option>' . "\n";
						echo '<option value="Asia/Beirut"' . ( ( $optintimezone == 'Asia/Beirut' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Beirut</option>' . "\n";
						echo '<option value="Africa/Cairo"' . ( ( $optintimezone == 'Africa/Cairo' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Cairo</option>' . "\n";
						echo '<option value="Asia/Gaza"' . ( ( $optintimezone == 'Asia/Gaza' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Gaza</option>' . "\n";
						echo '<option value="Africa/Blantyre"' . ( ( $optintimezone == 'Africa/Blantyre' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Harare, Pretoria</option>' . "\n";
						echo '<option value="Asia/Jerusalem"' . ( ( $optintimezone == 'Asia/Jerusalem' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Jerusalem</option>' . "\n";
						echo '<option value="Europe/Minsk"' . ( ( $optintimezone == 'Europe/Minsk' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Minsk</option>' . "\n";
						echo '<option value="Asia/Damascus"' . ( ( $optintimezone == 'Asia/Damascus' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Syria</option>' . "\n";
						echo '<option value="Europe/Moscow"' . ( ( $optintimezone == 'Europe/Moscow' ) ? 'selected="selected"' : '' ) . '>(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>' . "\n";
						echo '<option value="Africa/Addis_Ababa"' . ( ( $optintimezone == 'Africa/Addis_Ababa' ) ? 'selected="selected"' : '' ) . '>(GMT+03:00) Nairobi</option>' . "\n";
						echo '<option value="Asia/Tehran"' . ( ( $optintimezone == 'Asia/Tehran' ) ? 'selected="selected"' : '' ) . '>(GMT+03:30) Tehran</option>' . "\n";
						echo '<option value="Asia/Dubai"' . ( ( $optintimezone == 'Asia/Dubai' ) ? 'selected="selected"' : '' ) . '>(GMT+04:00) Abu Dhabi, Muscat</option>' . "\n";
						echo '<option value="Asia/Yerevan"' . ( ( $optintimezone == 'Asia/Yerevan' ) ? 'selected="selected"' : '' ) . '>(GMT+04:00) Yerevan</option>' . "\n";
						echo '<option value="Asia/Kabul"' . ( ( $optintimezone == 'Asia/Kabul' ) ? 'selected="selected"' : '' ) . '>(GMT+04:30) Kabul</option>' . "\n";
						echo '<option value="Asia/Yekaterinburg"' . ( ( $optintimezone == 'Asia/Yekaterinburg' ) ? 'selected="selected"' : '' ) . '>(GMT+05:00) Ekaterinburg</option>' . "\n";
						echo '<option value="Asia/Tashkent"' . ( ( $optintimezone == 'Asia/Tashkent' ) ? 'selected="selected"' : '' ) . '>(GMT+05:00) Tashkent</option>' . "\n";
						echo '<option value="Asia/Kolkata"' . ( ( $optintimezone == 'Asia/Kolkata' ) ? 'selected="selected"' : '' ) . '>(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>' . "\n";
						echo '<option value="Asia/Katmandu"' . ( ( $optintimezone == 'Asia/Katmandu' ) ? 'selected="selected"' : '' ) . '>(GMT+05:45) Kathmandu</option>' . "\n";
						echo '<option value="Asia/Dhaka"' . ( ( $optintimezone == 'Asia/Dhaka' ) ? 'selected="selected"' : '' ) . '>(GMT+06:00) Astana, Dhaka</option>' . "\n";
						echo '<option value="Asia/Novosibirsk"' . ( ( $optintimezone == 'Asia/Novosibirsk' ) ? 'selected="selected"' : '' ) . '>(GMT+06:00) Novosibirsk</option>' . "\n";
						echo '<option value="Asia/Rangoon"' . ( ( $optintimezone == 'Asia/Rangoon' ) ? 'selected="selected"' : '' ) . '>(GMT+06:30) Yangon (Rangoon)</option>' . "\n";
						echo '<option value="Asia/Bangkok"' . ( ( $optintimezone == 'Asia/Bangkok' ) ? 'selected="selected"' : '' ) . '>(GMT+07:00) Bangkok, Hanoi, Jakarta</option>' . "\n";
						echo '<option value="Asia/Krasnoyarsk"' . ( ( $optintimezone == 'Asia/Krasnoyarsk' ) ? 'selected="selected"' : '' ) . '>(GMT+07:00) Krasnoyarsk</option>' . "\n";
						echo '<option value="Asia/Hong_Kong"' . ( ( $optintimezone == 'Asia/Hong_Kong' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>' . "\n";
						echo '<option value="Asia/Irkutsk"' . ( ( $optintimezone == 'Asia/Irkutsk' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Irkutsk, Ulaan Bataar</option>' . "\n";
						echo '<option value="Australia/Perth"' . ( ( $optintimezone == 'Australia/Perth' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Perth</option>' . "\n";
						echo '<option value="Australia/Eucla"' . ( ( $optintimezone == 'Australia/Eucla' ) ? 'selected="selected"' : '' ) . '>(GMT+08:45) Eucla</option>' . "\n";
						echo '<option value="Asia/Tokyo"' . ( ( $optintimezone == 'Asia/Tokyo' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Osaka, Sapporo, Tokyo</option>' . "\n";
						echo '<option value="Asia/Seoul"' . ( ( $optintimezone == 'Asia/Seoul' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Seoul</option>' . "\n";
						echo '<option value="Asia/Yakutsk"' . ( ( $optintimezone == 'Asia/Yakutsk' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Yakutsk</option>' . "\n";
						echo '<option value="Australia/Adelaide"' . ( ( $optintimezone == 'Australia/Adelaide' ) ? 'selected="selected"' : '' ) . '>(GMT+09:30) Adelaide</option>' . "\n";
						echo '<option value="Australia/Darwin"' . ( ( $optintimezone == 'Australia/Darwin' ) ? 'selected="selected"' : '' ) . '>(GMT+09:30) Darwin</option>' . "\n";
						echo '<option value="Australia/Brisbane"' . ( ( $optintimezone == 'Australia/Brisbane' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Brisbane</option>' . "\n";
						echo '<option value="Australia/Hobart"' . ( ( $optintimezone == 'Australia/Hobart' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Hobart</option>' . "\n";
						echo '<option value="Asia/Vladivostok"' . ( ( $optintimezone == 'Asia/Vladivostok' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Vladivostok</option>' . "\n";
						echo '<option value="Australia/Lord_Howe"' . ( ( $optintimezone == 'Australia/Lord_Howe' ) ? 'selected="selected"' : '' ) . '>(GMT+10:30) Lord Howe Island</option>' . "\n";
						echo '<option value="Etc/GMT-11"' . ( ( $optintimezone == 'Etc/GMT-11' ) ? 'selected="selected"' : '' ) . '>(GMT+11:00) Solomon Is., New Caledonia</option>' . "\n";
						echo '<option value="Asia/Magadan"' . ( ( $optintimezone == 'Asia/Magadan' ) ? 'selected="selected"' : '' ) . '>(GMT+11:00) Magadan</option>' . "\n";
						echo '<option value="Pacific/Norfolk"' . ( ( $optintimezone == 'Pacific/Norfolk' ) ? 'selected="selected"' : '' ) . '>(GMT+11:30) Norfolk Island</option>' . "\n";
						echo '<option value="Asia/Anadyr"' . ( ( $optintimezone == 'Asia/Anadyr' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Anadyr, Kamchatka</option>' . "\n";
						echo '<option value="Pacific/Auckland"' . ( ( $optintimezone == 'Pacific/Auckland' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Auckland, Wellington</option>' . "\n";
						echo '<option value="Etc/GMT-12"' . ( ( $optintimezone == 'Etc/GMT-12' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>' . "\n";
						echo '<option value="Pacific/Chatham"' . ( ( $optintimezone == 'Pacific/Chatham' ) ? 'selected="selected"' : '' ) . '>(GMT+12:45) Chatham Islands</option>' . "\n";
						echo '<option value="Pacific/Tongatapu"' . ( ( $optintimezone == 'Pacific/Tongatapu' ) ? 'selected="selected"' : '' ) . '>(GMT+13:00) Nuku\'alofa</option>' . "\n";
						echo '<option value="Pacific/Kiritimati"' . ( ( $optintimezone == 'Pacific/Kiritimati' ) ? 'selected="selected"' : '' ) . '>(GMT+14:00) Kiritimati</option>' . "\n";
						echo '</select>';
						echo '</div>';
						break;

					case 'shortselect':
						echo '<div id=\'wpoptin_shortselect\' style=" width: 175px; border:1px solid #ccc; padding:10px 10px 0px 10px;border-radius:2px; margin-right: 15px; margin-bottom: 15px; float: left;">';
						echo '<label for="', $field['id'], '" style="border-bottom: 1px solid #ccc; padding-bottom: 10px;" >', $field['name'], '</label>';
						echo '<select style="margin-right: 15px; margin-top: 5px; float: left;" name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						echo '<p style="width: 100%;">', $field['desc'],'</p>';
						echo '</div>';
						break;

					case 'shortselect2':
						echo '<div id=\'wpoptin_shortselect\' style=" width: 175px; border:1px solid #ccc; padding:10px 10px 0px 10px;border-radius:2px; margin-right: 15px; margin-bottom: 15px; float: left;">';
						echo '<label for="', $field['id'], '" style="border-bottom: 1px solid #ccc; padding-bottom: 10px;" >', $field['name'], '</label>';
						echo '<select style="margin-right: 15px; margin-top: 5px; float: left;" name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						echo '<p style="width: 100%;">', $field['desc'],'</p>';
						echo '</div><div style="clear: both"></div>';
						break;

					case 'radio':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
						}
						break;

					case 'checkboxmedia':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						foreach ( $field['options'] as $option ) {
							echo '<input type="checkbox" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
						}
						break;

					case 'checkbox':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						break;

					case 'checkboxtheme':
						echo '<div id="theme_image_checkbox" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpoptin_theme_basic\');uncheck_checkbox(\'wpoptin_theme_modern\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxtheme1':
						echo '<div id="theme_image_checkbox1" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpoptin_theme_yours\');uncheck_checkbox(\'wpoptin_theme_modern\');"type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxtheme2':
						echo '<div id="theme_image_checkbox2" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpoptin_theme_yours\');uncheck_checkbox(\'wpoptin_theme_basic\');"type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxthemeoriginal':
						echo '<div id="theme_image_checkbox" style="float: left; width: 125px; height: 125px"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /></div>';
						break;

					// optin TIME CHECKBOX DAYS OF WEEK

					case 'checkboxtime':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxtimefirst':
						echo '<div id="recurring_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxtimelast':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						echo '</div>';
						break;

					// MEDIA CHECKBOXES

					case 'checkboxvideo':
						echo '<!--Media Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div(\'video_box\',\'wpoptin_show_video\');uncheck_checkbox(\'wpoptin_show_audio\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxaudio':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div(\'audio_box\',\'wpoptin_show_audio\');uncheck_checkbox(\'wpoptin_show_video\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// TIMEZONE CHECKBOXES

					case 'checkboxlocaltime':
						echo '<!--Timezone Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="hide_div4(\'admintimebox\',\'wpoptin_admintime\');uncheck_checkbox(\'wpoptin_admintime\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxadmintime':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div4(\'admintimebox\',\'wpoptin_admintime\');uncheck_checkbox(\'wpoptin_localtime\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// optin CHECKBOXES

					case 'checkboxoptin':
						echo '<!--optin Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div3(\'onetime_box\',\'wpoptin_onetime_optin\');uncheck_checkbox(\'wpoptin_recurring_optin\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxoptins':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div3(\'recurring_box\',\'wpoptin_recurring_optin\');uncheck_checkbox(\'wpoptin_onetime_optin\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// AUTORESPONDER CHECKBOXES

					case 'checkboxaweber':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'aweber_box\',\'wpoptin_aweber\');uncheck_checkbox(\'wpoptin_oneshoppingcart\');uncheck_checkbox(\'wpoptin_getresponse\');uncheck_checkbox(\'wpoptin_arp3\');uncheck_checkbox(\'wpoptin_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkbox1shoppingcart':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'1shoppingcart_box\',\'wpoptin_oneshoppingcart\');uncheck_checkbox(\'wpoptin_aweber\');uncheck_checkbox(\'wpoptin_getresponse\');uncheck_checkbox(\'wpoptin_arp3\');uncheck_checkbox(\'wpoptin_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxgetresponse':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'getresponse_box\',\'wpoptin_getresponse\');uncheck_checkbox(\'wpoptin_aweber\');uncheck_checkbox(\'wpoptin_oneshoppingcart\');uncheck_checkbox(\'wpoptin_arp3\');uncheck_checkbox(\'wpoptin_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxarp3':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'arp3_box\',\'wpoptin_arp3\');uncheck_checkbox(\'wpoptin_aweber\');uncheck_checkbox(\'wpoptin_getresponse\');uncheck_checkbox(\'wpoptin_oneshoppingcart\');uncheck_checkbox(\'wpoptin_other\');"name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					case 'checkboxother':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'other_box\',\'wpoptin_other\');uncheck_checkbox(\'wpoptin_aweber\');uncheck_checkbox(\'wpoptin_getresponse\');uncheck_checkbox(\'wpoptin_oneshoppingcart\');uncheck_checkbox(\'wpoptin_arp3\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// BUTTON CHECKBOXES

					case 'checkboxorange':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpoptin_button_green\');uncheck_checkbox(\'wpoptin_button_blue\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img  src="' . WP_PLUGIN_URL . '/wp-optin/images/orangebutton.png"/>' . "\n";
						break;

					case 'checkboxgreen':
						echo '<div style="margin: 10px 0px"><input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpoptin_button_orange\');uncheck_checkbox(\'wpoptin_button_blue\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img src="' . WP_PLUGIN_URL . '/wp-optin/images/greenbutton.png"/></div>' . "\n";
						break;

					case 'checkboxblue':
						echo '<input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpoptin_button_orange\');uncheck_checkbox(\'wpoptin_button_green\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img  src="' . WP_PLUGIN_URL . '/wp-optin/images/bluebutton.png"/>' . "\n";
						break;
				}
			}
		endif;
	}
	echo '</div>';
}

// *********************************************************************************** //
// SAVE METABOX AND SANITIZE INFORMATION
// *********************************************************************************** //

add_action( 'save_post', 'wpoptin_save_data' );

// Save data from meta box

function wpoptin_save_data( $post_id ) {
	global $meta_boxes_optin, $post_id;

	if ( ! isset( $_REQUEST['wpoptin_meta_box_nonce'] ) ) {
		return;
	}

	// verify nonce
	if ( ! wp_verify_nonce( $_POST['wpoptin_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	// check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	// check permissions
	if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $meta_boxes_optin as $meta_box_optin ) {
		foreach ( $meta_box_optin['fields'] as $field ) {
			$old = get_post_meta( $post_id, $field['id'], true );
			$new = $_POST[ $field['id'] ];

			if ( $new && $new != $old ) {
				update_post_meta( $post_id, $field['id'], $new );
			} elseif ( '' == $new && $old ) {
				delete_post_meta( $post_id, $field['id'], $old );
			}
		}
	}
}


// *********************************************************************************** //
// FOOTER SCRIPTS
// *********************************************************************************** //


add_action( 'admin_footer', 'wpoptin_enqueue_scripts' );

function wpoptin_enqueue_scripts() {
	global $post;
	global $post_id;

	$showvideo       = get_post_meta( $post_id, 'wpoptin_show_video', true );
	$showaudio       = get_post_meta( $post_id, 'wpoptin_show_audio', true );
	$aweber          = get_post_meta( $post_id, 'wpoptin_aweber', true );
	$oneshoppingcart = get_post_meta( $post_id, 'wpoptin_oneshoppingcart', true );
	$arp3            = get_post_meta( $post_id, 'wpoptin_arp3', true );
	$getresponse     = get_post_meta( $post_id, 'wpoptin_getresponse', true );
	$other           = get_post_meta( $post_id, 'wpoptin_other', true );
	$onetime         = get_post_meta( $post_id, 'wpoptin_onetime_optin', true );
	$recurring       = get_post_meta( $post_id, 'wpoptin_recurring_optin', true );

	if ( $post && $post->post_type && $post->post_type == 'wpoptin' ) {

		if ( $showvideo == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('video_box').style.display='block';</script>";
		} elseif ( $showaudio == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('audio_box').style.display='block';</script>";
		}
		if ( $aweber == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('aweber_box').style.display='block';</script>";
		} elseif ( $oneshoppingcart == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('1shoppingcart_box').style.display='block';</script>";
		} elseif ( $getresponse == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('getresponse_box').style.display='block';</script>";
		} elseif ( $arp3 == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('arp3_box').style.display='block';</script>";
		} elseif ( $other == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('other_box').style.display='block';</script>";
		}
		if ( $onetime == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('onetime_box').style.display='block';</script>"; } elseif ( $recurring == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('recurring_box').style.display='block';</script>"; }
			?>
	  

	<script type='text/javascript'>
		function uncheck_checkbox(fieldname){
			var div = document.forms.post.elements[fieldname];
			div.checked=false;
		}

		function hide_div() {
			var div = new Array('video_box','audio_box');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}

		function hide_div2() {
			var div = new Array('aweber_box','1shoppingcart_box','getresponse_box','arp3_box','other_box');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}

		}
		function hide_div3() {
			var div = new Array('onetime_box','recurring_box');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}

		function hide_div4(){
			var div = new Array('admintimebox');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}
		function show_div(div,checkdiv){
			hide_div();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true) {
				document.getElementById(div).style.display='block';
			}
			else{
				document.getElementById(div).style.display='none';
			}
		}
		function show_div2(div,checkdiv) {
			hide_div2();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true){
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
		function show_div3(div,checkdiv) {
			hide_div3();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true){
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
		function show_div4(div,checkdiv) {
			hide_div4();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true) {
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
	</script>
<?php }
}

?>
