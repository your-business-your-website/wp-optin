<?php /*** WPoptin optin TEMPLATE ***/ ?>


<?php
if ( ! function_exists( 'is_ssl' ) ) {
	function is_ssl() {
		if ( isset( $_SERVER['HTTPS'] ) ) {
			if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
				return true;
			}
			if ( '1' == $_SERVER['HTTPS'] ) {
				return true;
			}
		} elseif ( isset( $_SERVER['SERVER_PORT'] ) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
			return true;
		}
		return false;
	}
}

if ( version_compare( get_bloginfo( 'version' ), '3.0', '<' ) && is_ssl() ) {
	$wp_content_url = str_replace( 'http://', 'https://', get_option( 'siteurl' ) );
} else {
	$wp_content_url = get_option( 'siteurl' );
}
$wp_content_url .= '/wp-content';
$wp_content_dir  = ABSPATH . 'wp-content';
$wp_plugin_url   = $wp_content_url . '/plugins';
$wp_plugin_dir   = $wp_content_dir . '/plugins';
$wpmu_plugin_url = $wp_content_url . '/mu-plugins';
$wpmu_plugin_dir = $wp_content_dir . '/mu-plugins';

?>

<?php
global $post;
$post_id     = $post->ID;
$yourtheme   = get_post_meta( $post_id, 'wpoptin_theme_yours', true );
$basictheme  = get_post_meta( $post_id, 'wpoptin_theme_basic', true );
$moderntheme = get_post_meta( $post_id, 'wpoptin_theme_modern', true );
$themecolor  = get_post_meta( $post_id, 'wpoptin_theme_color', true );

if ( $yourtheme == 'on' ) {
	get_header();
}
if ( $basictheme == 'on' ) {
	?>
		<html>
		<link rel='stylesheet' id='wpoptinstylesheet-css'  href='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/templates/basic/';
		?>
		style.css' type='text/css' media='all' />
		<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		supersized.3.1.3.core.min.js?ver=1.0'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		init.js?ver=1.0'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		misc.js?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		countdown.php?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		flowplayer-3.2.6.min.js?ver=3.6-beta4-24596'></script>

		<header style="background-color: <?php echo $themecolor; ?>;">
		<div id="headinner">
		<div id="optinlogo"><img src="
		<?php
		global $post;
		$post_id = $post->ID;
		$image   = get_post_meta( $post_id, 'wpoptin_logo', true );
		$image   = do_shortcode( $image );
		$image   = wp_get_attachment_image_src( $image, 'full' );
		$image   = $image[0];
		if ( empty( $image ) ) {
			echo ''; } else {
			echo $image;
			}
			?>
		"/></div>
		</div>
		<div style="clear: both;"></div>
		</header>
		<body>
		<div id="pageshadow">

	<?php
}

if ( $moderntheme == 'on' ) {
	?>
		<html>
		<link rel='stylesheet' id='wpoptinstylesheet-css'  href='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/templates/modern/';
		?>
		style.css' type='text/css' media='all' />
		<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		supersized.3.1.3.core.min.js?ver=1.0'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		init.js?ver=1.0'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		misc.js?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		countdown.php?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/js/';
		?>
		flowplayer-3.2.6.min.js?ver=3.6-beta4-24596'></script>

		<header style="background-color: <?php echo $themecolor; ?>;">
		</header>
		<body style="background-color: <?php echo $themecolor; ?>;">
			<div id="bodycolor">
		<?php
		echo '<center>';
		echo wpautop( get_post_meta( $post_id, 'wpoptin_main_headline', true ) );
		echo '</center>';
		?>

<?php } ?>

<?php
$theme = get_current_theme();
if ( 'Inspire' == $theme ) {
	woo_crumbs();
	echo '</div><!-- /#top -->';
}
?>
	
<?php
$theme = get_current_theme();
if ( 'Optimize' == $theme ) {
	?>

	<div id="featured">
		<div id="page-title" class="col-full">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div id="breadcrumb">
		<div class="col-full">
			<div class="fl">
			<?php
			if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb( '', '' );}
			?>
			</div>
			<a class="subscribe fr" href="
			<?php
			if ( get_option( 'woo_feedburner_url' ) <> '' ) {
				echo get_option( 'woo_feedburner_url' );
			} else {
				echo get_bloginfo_rss( 'rss2_url' ); }
			?>
			">
				<img src="<?php bloginfo( 'template_directory' ); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
			</a>        
		</div>
	</div>              
</div><!-- /#top -->  

<?php } ?>

<?php
global $post;
$post_id = $post->ID;

	echo '<script type="text/javascript" src="';
	echo $wp_plugin_url;
echo '/wp-optin/js/countdown.php?timezone=';
if ( get_post_meta( $post_id, 'wpoptin_admintime', true ) == 'on' ) {
	echo get_post_meta( $post_id, 'wpoptin_optin_timezone', true );
}
		echo '"></script>';
?>

<!-- Custom Styling2 --> 
<style type="text/css">
.optinpagebutton { background-image:url('<?php
global $post;
$post_id = $post->ID;

$button_orange = get_post_meta( $post_id, 'wpoptin_button_orange', true );
$button_green  = get_post_meta( $post_id, 'wpoptin_button_green', true );
$button_blue   = get_post_meta( $post_id, 'wpoptin_button_blue', true );

if ( $button_orange == 'on' ) {
	echo $wp_plugin_url;
	echo '/wp-optin/images/orangebutton.png)';
} else {
	echo '';
}

if ( $button_green == 'on' ) {
		echo $wp_plugin_url;
		echo '/wp-optin/images/greenbutton.png)'; 
} else {
	echo '';
}

if ( $button_blue == 'on' ) {
		echo $wp_plugin_url;
		echo '/wp-optin/images/bluebutton.png)'; 
} else {
	echo '';
}
?>')!important;}
</style>

<?php
$values = $_GET['optin'];
if ( $values == '' ) {
	?>

<div id="optin_wrapper"> <!-- START WRAPPER OPTIN -->
	<!-- ****************************************************************** -->
	<!-- HEADLINE -->
	<!-- ****************************************************************** -->
	
	<?php
	global $post;
	$post_id    = $post->ID;
	$yourtheme  = get_post_meta( $post_id, 'wpoptin_theme_yours', true );
	$basictheme = get_post_meta( $post_id, 'wpoptin_theme_basic', true );

	if ( $yourtheme == 'on' ) {
		echo '<div id="optin_headline">';
		echo wpautop( get_post_meta( $post_id, 'wpoptin_main_headline', true ) );
		echo '</div>';
	}
	if ( $basictheme == 'on' ) {
		?>
		<img class="basic-head-img" 
		<?php
		$basictheme = get_post_meta( $post_id, 'wpoptin_theme_basic', true );
		if ( $basictheme == 'on' ) {
			echo 'style="margin-bottom: 20px;"';}
		?>
		 src="
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/templates/basic/images/reg1.png';
		?>
		" width="980px" height="47px">
	<?php } ?>
	
	<div id="optin_content">
		<div id="optin_video">
		<!-- ****************************************************************** -->
		<!-- OPTIONAL VIDEO AND AUDIO -->
		<!-- ****************************************************************** -->
		<?php
		global $post;
		$post_id   = $post->ID;
		$showvideo = get_post_meta( $post_id, 'wpoptin_show_video', true );
		$showaudio = get_post_meta( $post_id, 'wpoptin_show_audio', true );
		if ( '' !== $showvideo || '' !== $showaudio ) {
			if ( 'on' == $showvideo ) {

				$video         = get_post_meta( $post_id, 'wpoptin_video_url', true );
				$video_id      = VideoUrlParser::get_url_id( $video );
				$video_service = VideoUrlParser::identify_service( $video );

				if ( 'vimeo' === $video_service ) {
					echo '<div style="position:relative;width:100%;height:0;padding-bottom:56.27198%;"><iframe style="position:absolute;top:0;left:0;width:100%;height:100%;" width="800px" height="600px" src="https://player.vimeo.com/video/' . $video_id . '?" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
				} elseif ( 'youtube' === $video_service ) {
					echo '<div style="position:relative;width:100%;height:0;padding-bottom:56.27198%;"><iframe style="position:absolute;top:0;left:0;width:100%;height:100%;" width="800" height="600" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe></div>';
				} elseif ( 'adilo' === $video_service ) {
					echo '<div class="motion_popover" data-id="' . $video_id . '" style="position:relative;width:100%;height:0;padding-bottom:56.27198%; data-type="thumbnail"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%" allowtransparency="true" src="https://adilo.bigcommand.com/watch/' . $video_id . '" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" scrolling="no" frameborder="0"></iframe></div>';
				} elseif ( 'fastly' === $video_service ) {
					echo '<div style="position:relative;width:100%;height:0;padding-bottom:56.27198%;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%" allowtransparency="true" class="_vs_ictr_player" src="https://swiftcdn6.global.ssl.fastly.net/projects/614d63de8304c/index.html?cb=' . $video_id . '" allow="autoplay *" scrolling="no" width="550" height="405" frameborder="0"></iframe></div>';
				} elseif ( 'video' === $video_service ) {
					echo '<video width="100%" height="100%" controls><source src="' . $video_id . '"></video>';
				}
			} else {
				// start audio button
				if ( strstr( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPod' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) ) {
					echo "<audio width='100' height='30' controls='controls' autoplay='autoplay'><source src='";
					echo get_post_meta( $post_id, 'optin_mp3_url', true );
					echo "'/>";
				} else {
					echo "<script type='text/javascript' src='";
					echo $wp_plugin_url;
					echo "/wp-optin/player/swfobject.js'></script><div id='mediaspace' style='width:200px;height:32px;border:1px solid #ccc;'>This text will be replaced</div><script type='text/javascript'>var so = new SWFObject('";
					echo $wp_plugin_url;
					echo "/wp-optin/player/player.swf','ply','200','32','9','#000000'); so.addParam('allowfullscreen','true'); so.addParam('allowscriptaccess','always');	so.addParam('wmode','opaque'); so.addVariable('file','";
					echo get_post_meta( $post_id, 'optin_mp3_url', true );
					echo "');so.addVariable('volume','100');	so.addVariable('autostart','true');	so.addVariable('skin', '";
					echo $wp_plugin_url;
					echo "/wp-optin/player/stylish_slim.swf'); so.write('mediaspace');</script>"; }
			}
		} else {
			echo '';}
		?>
		</div><!-- END optin VIDEO -->

		<!-- ****************************************************************** -->
		<!-- TEXT BELOW VIDEO -->
		<!-- ****************************************************************** -->

		<?php
		global $post;
		$post_id = $post->ID;
			echo wpautop( get_post_meta( $post_id, 'wpoptin_optional_text', true ) );
		?>

	</div><!-- END optinCONTENT -->
	
	<!-- ****************************************************************** -->
	<!-- optin SIDEBAR -->
	<!-- ****************************************************************** -->

	<div id="optinsidebar" 
	<?php
	$themecolor = get_post_meta( $post_id, 'wpoptin_theme_color', true );
	?>
	>
	<!-- START optin SIDEBAR -->

		<!-- ****************************************************************** -->
		<!-- optin BOX HEADLINE -->
		<!-- ****************************************************************** -->
		<div id="sidebarhead" style="background-color: 
		<?php
		$themecolor  = get_post_meta( $post_id, 'wpoptin_theme_color', true );
		$moderntheme = get_post_meta( $post_id, 'wpoptin_theme_modern', true );
		if ( ( $moderntheme == 'on' ) || ( $yourtheme == 'on' ) ) {
			echo 'transparent';
		} else {
			echo $themecolor;}
		?>
		;">
		<h2>
		<?php
		global $post;
		$post_id = $post->ID;
		echo get_post_meta( $post_id, 'wpoptin_box_headline', true );
		?>
		</h2>
		</div>
		<?php
		$moderntheme = get_post_meta( $post_id, 'wpoptin_theme_modern', true );
		$yourtheme   = get_post_meta( $post_id, 'wpoptin_theme_yours', true );
		if ( ( $moderntheme == 'on' ) || ( $yourtheme == 'on' ) ) {
			echo '';
		} else {
			echo '<div style="width: 0; height: 0; border-left: 20px solid transparent; border-right: 20px solid transparent; margin: 0px auto; border-top: 20px solid <?php $themecolor = get_post_meta($post_id,\'wpoptin_theme_color\',true); echo $themecolor;} ?>;"></div>';
		}
		?>
		<div id="sidebarcontent">
		<!-- ****************************************************************** -->
		<!-- optin BOX DECRIPTION -->
		<!-- ****************************************************************** -->
		<?php
		global $post;
		$post_id = $post->ID;
		echo wpautop( get_post_meta( $post_id, 'wpoptin_box_description', true ) );
		?>

		<!-- ****************************************************************** -->
		<!-- DATE AND TIME DROP DOWN BOXES -->
		<!-- ****************************************************************** -->

		<script type="text/javascript">
			function copyEmail(){
				var f1 = document.getElementById("email");
				var f2 = document.getElementById("autoemail");
				f2.value = f1.value;
			}
			function copyName(){
				var f3 = document.getElementById("first_name");
				var f4 = document.getElementById("autoname");
				f4.value = f3.value;
			}
		</script>	

<script type="text/javascript">
$(document).ready(function() {

	$("#form1").submit(function(e) {
		var date = $("#date").val();
		if (date == "-1") {
			e.preventDefault();
			alert("Please Select optin Date");
		} else {
			var time = $("#time").val();
			if (time == "-1") {
				e.preventDefault();
				alert("Please Select optin Time");
			} else {
				var name = $("#first_name").val();
				if (name == "Enter Your First Name") {
					e.preventDefault();
					alert("Please Enter Your First Name");
				} else {
					var email = $("#email").val();
					if (email == "Type In Your Email Address") {
						e.preventDefault();
						alert("Please Type In Your Email Address");
					} 
				}
			}
		}				
	});

	$(function(){
		$('input.optinpagebutton').click(function(event){
			$.post($("#form2").attr("action"), $("#form2").serialize(),
				function(data) {
				$("#msg").append(data);
			});		
		});
	});

	$(function(){
		$('input.optinpagebutton').click(function(event){
			event.preventDefault();
			var button = this;
			window.setTimeout(function(form){
				$(form).submit();
			}, 500, $(button).closest('form'));
		});
	});

});
</script>

		<?php
			global $post;
			$post_id         = $post->ID;
			$aweber          = get_post_meta( $post_id, 'wpoptin_aweber', true );
			$oneshoppingcart = get_post_meta( $post_id, 'wpoptin_oneshoppingcart', true );
			$arp3            = get_post_meta( $post_id, 'wpoptin_arp3', true );
			$getresponse     = get_post_meta( $post_id, 'wpoptin_getresponse', true );
			$other           = get_post_meta( $post_id, 'wpoptin_other', true );
			$page            = get_permalink( $_REQUEST['posted'] );

			// Aweber Form Set
		if ( ( get_post_meta( $post_id, 'wpoptin_recurring_optin', true ) == 'on' ) ) {
				echo '<form name="form1" id="form1" action="">';
				echo '<input type="hidden" name="optin" id="optin" value="processing1" />';
				echo '<input type="hidden" name="type" id="type" value="r" /> ';
		}
		if ( ( get_post_meta( $post_id, 'wpoptin_onetime_optin', true ) == 'on' ) ) {
				echo '<form name="form1" id="form1" action="">' . "\n";
				echo '<input type="hidden" name="optin" id="optin" value="processing1" />' . "\n";
				echo '<input type="hidden" name="type" id="type" value="o" /> ' . "\n";
				echo '<input type="hidden" name="date" id="date" value="';
				echo get_post_meta( $post_id, 'wpoptin_optin_year', true );
				echo '-';
				echo get_post_meta( $post_id, 'wpoptin_optin_month', true );
				echo '-';
				echo get_post_meta( $post_id, 'wpoptin_optin_day', true );
				echo '" />' . "\n";
				echo '<input type="hidden" name="time" id="time" value="';
				echo get_post_meta( $post_id, 'wpoptin_optin_hour', true );
				echo '" />' . "\n";
		}

		?>
		
		<!-- RECURRING optin ON -->
		
		<?php if ( get_post_meta( $post_id, 'wpoptin_recurring_optin', true ) == 'on' ) { ?>

<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$(document).ready(function() {
		$("#date").html($('#date option').sort(function(x, y) {
			return $(x).val() < $(y).val() ? -1 : 1;
		}))
		$("#date").get(0).selectedIndex = 0;
		e.preventDefault();
});
});//]]>  
</script>

<script type='text/javascript'>
window.onload = function(){
	selectCount = document.getElementById("date").length;
	if (selectCount == "2") {
		document.getElementById("date").options.length=2;
	}
	if (selectCount == "3") {
		document.getElementById("date").options.length=3;
	}
	if (selectCount >= "4") {
		document.getElementById("date").options.length=4;
	}
}
</script>

			<div style="text-align: center;">
				<?php
				global $post;
				$post_id            = $post->ID;
					$optinsunday    = get_post_meta( $post_id, 'wpoptin_sunday', true );
					$optinmonday    = get_post_meta( $post_id, 'wpoptin_monday', true );
					$optintuesday   = get_post_meta( $post_id, 'wpoptin_tuesday', true );
					$optinwednesday = get_post_meta( $post_id, 'wpoptin_wednesday', true );
					$optinthursday  = get_post_meta( $post_id, 'wpoptin_thursday', true );
					$optinfriday    = get_post_meta( $post_id, 'wpoptin_friday', true );
					$optinsaturday  = get_post_meta( $post_id, 'wpoptin_saturday', true );
				?>
				<?php
				if ( $moderntheme == 'on' ) {
					echo '<div class="optintext"><b>1.)</b> Which Day Do You Want To Attend?</div>';
				}
				?>
				<select id ="date" name="date">
					<option value="-1">Select The optin Date</option>
					
					<?php
					if ( $optinsunday == 'on' && ( ( date( N ) == '4' ) || ( date( N ) == '5' ) || ( date( N ) == '6' ) || ( date( N ) == '7' ) ) ) {
						?>
						<option value="
						<?php
						$sundaytime = strtotime( 'this Sunday ' );
						echo date( 'Y-m-d', $sundaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Sunday' ) ); ?></option> <?php } ?>
					<?php
					if ( $optinsunday == 'on' && ( ( date( N ) == '1' ) || ( date( N ) == '2' ) || ( date( N ) == '3' ) ) ) {
						?>
						<option value="
						<?php
						$sundaytime = strtotime( 'next Sunday ' );
						echo date( 'Y-m-d', $sundaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Sunday' ) ); ?></option><?php } ?>
					
					<?php
					if ( $optinmonday == 'on' && ( ( date( N ) == '5' ) || ( date( N ) == '6' ) || ( date( N ) == '7' ) || ( date( N ) == '1' ) ) ) {
						?>
						<option value="
						<?php
						$mondaytime = strtotime( 'this Monday' );
						echo date( 'Y-m-d', $mondaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Monday' ) ); ?></option> <?php } ?>
					<?php
					if ( $optinmonday == 'on' && ( ( date( N ) == '2' ) || ( date( N ) == '3' ) || ( date( N ) == '4' ) ) ) {
						?>
						<option value="
						<?php
						$mondaytime = strtotime( 'next Monday' );
						echo date( 'Y-m-d', $mondaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Monday' ) ); ?></option> <?php } ?>

					<?php
					if ( $optintuesday == 'on' && ( ( date( N ) == '6' ) || ( date( N ) == '7' ) || ( date( N ) == '1' ) || ( date( N ) == '2' ) ) ) {
						?>
						<option value="
						<?php
						$tuesdaytime = strtotime( 'this Tuesday' );
						echo date( 'Y-m-d', $tuesdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Tuesday' ) ); ?></option> <?php } ?>
					<?php
					if ( $optintuesday == 'on' && ( ( date( N ) == '3' ) || ( date( N ) == '4' ) || ( date( N ) == '5' ) ) ) {
						?>
						<option value="
						<?php
						$tuesdaytime = strtotime( 'next Tuesday' );
						echo date( 'Y-m-d', $tuesdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Tuesday' ) ); ?></option> <?php } ?>

					<?php
					if ( $optinwednesday == 'on' && ( ( date( N ) == '7' ) || ( date( N ) == '1' ) || ( date( N ) == '2' ) || ( date( N ) == '3' ) ) ) {
						?>
						<option value="
						<?php
						$wednesdaytime = strtotime( 'this Wednesday' );
						echo date( 'Y-m-d', $wednesdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Wednesday' ) ); ?></option> <?php } ?>
					<?php
					if ( $optinwednesday == 'on' && ( ( date( N ) == '4' ) || ( date( N ) == '5' ) || ( date( N ) == '6' ) ) ) {
						?>
						<option value="
						<?php
						$wednesdaytime = strtotime( 'next Wednesday' );
						echo date( 'Y-m-d', $wednesdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Wednesday' ) ); ?></option> <?php } ?>

					<?php
					if ( $optinthursday == 'on' && ( ( date( N ) == '1' ) || ( date( N ) == '2' ) || ( date( N ) == '3' ) || ( date( N ) == '4' ) ) ) {
						?>
						<option value="
						<?php
						$thursdaytime = strtotime( 'this Thursday' );
						echo date( 'Y-m-d', $thursdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Thursday' ) ); ?></option><?php } ?>
					<?php
					if ( $optinthursday == 'on' && ( ( date( N ) == '5' ) || ( date( N ) == '6' ) || ( date( N ) == '7' ) ) ) {
						?>
						<option value="
						<?php
						$thursdaytime = strtotime( 'next Thursday' );
						echo date( 'Y-m-d', $thursdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Thursday' ) ); ?></option><?php } ?>

					<?php
					if ( $optinfriday == 'on' && ( ( date( N ) == '2' ) || ( date( N ) == '3' ) || ( date( N ) == '4' ) || ( date( N ) == '5' ) ) ) {
						?>
						<option value="
						<?php
						$fridaytime = strtotime( 'this Friday' );
						echo date( 'Y-m-d', $fridaytime );
						?>
"> <?php echo date( 'l F jS, Y', strtotime( 'this Friday' ) ); ?></option> <?php } ?>
					<?php
					if ( $optinfriday == 'on' && ( ( date( N ) == '6' ) || ( date( N ) == '7' ) || ( date( N ) == '1' ) ) ) {
						?>
						<option value="
						<?php
						$fridaytime = strtotime( 'next Friday' );
						echo date( 'Y-m-d', $fridaytime );
						?>
"> <?php echo date( 'l F jS, Y', strtotime( 'next Friday' ) ); ?></option> <?php } ?>

					<?php
					if ( $optinsaturday == 'on' && ( ( date( N ) == '3' ) || ( date( N ) == '4' ) || ( date( N ) == '5' ) || ( date( N ) == '6' ) ) ) {
						?>
						<option value="
						<?php
						$saturdaytime = strtotime( 'this Saturday' );
						echo date( 'Y-m-d', $saturdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'this Saturday' ) ); ?></option><?php } ?>
					<?php
					if ( $optinsaturday == 'on' && ( ( date( N ) == '7' ) || ( date( N ) == '1' ) || ( date( N ) == '2' ) ) ) {
						?>
						<option value="
						<?php
						$saturdaytime = strtotime( 'next Saturday' );
						echo date( 'Y-m-d', $saturdaytime );
						?>
"><?php echo date( 'l F jS, Y', strtotime( 'next Saturday' ) ); ?></option><?php } ?>
				</select>

			<!-- ****************************************************************** -->
			<!-- SORTS DROP DOWN-->
			<!-- ****************************************************************** -->

				<script>
					var $dd = $('#date');
					if ($dd.length > 0) { 
						var selectedVal = $dd.val();
						var $options = $('option', $dd);
						var arrVals = [];
						$options.each(function(){
							arrVals.push({
								val: $(this).val(),
								text: $(this).text()
							});
						});
						arrVals.sort(function(a, b){
							return a.val - b.val;
						});
						for (var i = 0, l = arrVals.length; i < l; i++) {
							$($options[i]).val(arrVals[i].val).text(arrVals[i].text);
						}
						$dd.val(selectedVal);
					}
				</script>
				
			</div>
			<div style="clear:all;"></div>

			<!-- ****************************************************************** -->
			<!-- DYNAMIC TIME -->
			<!-- ****************************************************************** -->
				<?php
				if ( $moderntheme == 'on' ) {
					echo '<div class="optintext"><b>2.)</b> Which Time Is Best For You?</div>';
				}
				?>
					<div id="optin_local_time">Your Local Time Is: <script type="text/javascript">
<!--
	var currentTime = new Date()
	var hours = currentTime.getHours()
	var minutes = currentTime.getMinutes()

	if (minutes < 10)
	minutes = "0" + minutes

	var suffix = "AM";
	if (hours >= 12) {
	suffix = "PM";
	hours = hours - 12;
	}
	if (hours == 0) {
	hours = 12;
	}

	document.write("<b>" + hours + ":" + minutes + " " + suffix + "</b>")
//-->
</script></div>
<div style="clear: both;"></div>
			<select id="time" name="time">
			<option value="-1">Select The optin Time</option>
			<?php
				global $post;
				$post_id               = $post->ID;
				$recurring_optin_times = get_post_meta( $post_id, 'wpoptin_recurring_time', true );
				$admin_time            = get_post_meta( $post_id, 'wpoptin_admintime', true );
				$local_time            = get_post_meta( $post_id, 'wpoptin_localtime', true );
				$gmt_time_zone         = get_post_meta( $post_id, 'wpoptin_optin_timezone', true );

			if ( $local_time == 'on' ) { // IF LOCAL TIME
				if ( $recurring_optin_times != '' || $recurring_optin_time != '' ) {
					foreach ( $recurring_optin_times as $recurring_optin_time ) {
						?>
						<option value="<?php echo $recurring_optin_time; ?>">
							<?php
							if ( $recurring_optin_time == '1' ) {
								echo '1:00 AM Local Time'; }
							if ( $recurring_optin_time == '2' ) {
								echo '2:00 AM Local Time'; }
							if ( $recurring_optin_time == '3' ) {
								echo '3:00 AM Local Time'; }
							if ( $recurring_optin_time == '4' ) {
								echo '4:00 AM Local Time'; }
							if ( $recurring_optin_time == '5' ) {
								echo '5:00 AM Local Time'; }
							if ( $recurring_optin_time == '6' ) {
								echo '6:00 AM Local Time'; }
							if ( $recurring_optin_time == '7' ) {
								echo '7:00 AM Local Time'; }
							if ( $recurring_optin_time == '8' ) {
								echo '8:00 AM Local Time'; }
							if ( $recurring_optin_time == '9' ) {
								echo '9:00 AM Local Time'; }
							if ( $recurring_optin_time == '10' ) {
								echo '10:00 AM Local Time'; }
							if ( $recurring_optin_time == '11' ) {
								echo '11:00 AM Local Time'; }
							if ( $recurring_optin_time == '12' ) {
								echo '12:00 PM Local Time'; }
							if ( $recurring_optin_time == '13' ) {
								echo '1:00 PM Local Time'; }
							if ( $recurring_optin_time == '14' ) {
								echo '2:00 PM Local Time'; }
							if ( $recurring_optin_time == '15' ) {
								echo '3:00 PM Local Time'; }
							if ( $recurring_optin_time == '16' ) {
								echo '4:00 PM Local Time'; }
							if ( $recurring_optin_time == '17' ) {
								echo '5:00 PM Local Time'; }
							if ( $recurring_optin_time == '18' ) {
								echo '6:00 PM Local Time'; }
							if ( $recurring_optin_time == '19' ) {
								echo '7:00 PM Local Time'; }
							if ( $recurring_optin_time == '20' ) {
								echo '8:00 PM Local Time'; }
							if ( $recurring_optin_time == '21' ) {
								echo '9:00 PM Local Time'; }
							if ( $recurring_optin_time == '22' ) {
								echo '10:00 PM Local Time'; }
							if ( $recurring_optin_time == '23' ) {
								echo '11:00 PM Local Time'; }
							if ( $recurring_optin_time == '24' ) {
								echo '12:00 AM Local Time'; }
							?>
						</option>
						<?php
					}
				}
			}

			if ( $gmt_time_zone == 'Pacific/Midway' ) {
				$gmt_time_zone_set = '(GMT-11:00) Midway Island, Samoa'; }
			if ( $gmt_time_zone == 'America/Adak' ) {
				$gmt_time_zone_set = '(GMT-10:00) Hawaii-Aleutian'; }
			if ( $gmt_time_zone == 'Etc/GMT+10' ) {
				$gmt_time_zone_set = '(GMT-10:00) Hawaii'; }
			if ( $gmt_time_zone == 'Pacific/Marquesas' ) {
				$gmt_time_zone_set = '(GMT-09:30) Marquesas Islands'; }
			if ( $gmt_time_zone == 'Pacific/Gambier' ) {
				$gmt_time_zone_set = '(GMT-09:00) Gambier Islands'; }
			if ( $gmt_time_zone == 'America/Anchorage' ) {
				$gmt_time_zone_set = '(GMT-09:00) Alaska'; }
			if ( $gmt_time_zone == 'America/Ensenada' ) {
				$gmt_time_zone_set = '(GMT-08:00) Tijuana, Baja California'; }
			if ( $gmt_time_zone == 'Etc/GMT+8' ) {
				$gmt_time_zone_set = '(GMT-08:00) Pitcairn Islands'; }
			if ( $gmt_time_zone == 'America/Los_Angeles' ) {
				$gmt_time_zone_set = '(GMT-08:00) Pacific Time (US & Canada)'; }
			if ( $gmt_time_zone == 'America/Denver' ) {
				$gmt_time_zone_set = '(GMT-07:00) Mountain Time (US & Canada)'; }
			if ( $gmt_time_zone == 'America/Chihuahua' ) {
				$gmt_time_zone_set = '(GMT-07:00) Chihuahua, La Paz, Mazatlan'; }
			if ( $gmt_time_zone == 'America/Dawson_Creek' ) {
				$gmt_time_zone_set = '(GMT-07:00) Arizona'; }
			if ( $gmt_time_zone == 'America/Belize' ) {
				$gmt_time_zone_set = '(GMT-06:00) Saskatchewan, Central America'; }
			if ( $gmt_time_zone == 'America/Cancun' ) {
				$gmt_time_zone_set = '(GMT-06:00) Guadalajara, Mexico City, Monterrey'; }
			if ( $gmt_time_zone == 'Chile/EasterIsland' ) {
				$gmt_time_zone_set = '(GMT-06:00) Easter Island'; }
			if ( $gmt_time_zone == 'America/Chicago' ) {
				$gmt_time_zone_set = '(GMT-06:00) Central Time (US & Canada)'; }
			if ( $gmt_time_zone == 'America/New_York' ) {
				$gmt_time_zone_set = '(GMT-05:00) Eastern Time (US & Canada)'; }
			if ( $gmt_time_zone == 'America/Havana' ) {
				$gmt_time_zone_set = '(GMT-05:00) Cuba'; }
			if ( $gmt_time_zone == 'America/Bogota' ) {
				$gmt_time_zone_set = '(GMT-05:00) Bogota, Lima, Quito, Rio Branco'; }
			if ( $gmt_time_zone == 'America/Caracas' ) {
				$gmt_time_zone_set = '(GMT-04:30) Caracas'; }
			if ( $gmt_time_zone == 'America/Santiago' ) {
				$gmt_time_zone_set = '(GMT-04:00) Santiago'; }
			if ( $gmt_time_zone == 'America/La_Paz' ) {
				$gmt_time_zone_set = '(GMT-04:00) La Paz'; }
			if ( $gmt_time_zone == 'Atlantic/Stanley' ) {
				$gmt_time_zone_set = '(GMT-04:00) Faukland Islands'; }
			if ( $gmt_time_zone == 'America/Campo_Grande' ) {
				$gmt_time_zone_set = '(GMT-04:00) Brazil'; }
			if ( $gmt_time_zone == 'America/Goose_Bay' ) {
				$gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Goose Bay)'; }
			if ( $gmt_time_zone == 'America/Glace_Bay' ) {
				$gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Canada)'; }
			if ( $gmt_time_zone == 'America/St_Johns' ) {
				$gmt_time_zone_set = '(GMT-03:30) Newfoundland'; }
			if ( $gmt_time_zone == 'America/Araguaina' ) {
				$gmt_time_zone_set = '(GMT-03:00) UTC-3'; }
			if ( $gmt_time_zone == 'America/Montevideo' ) {
				$gmt_time_zone_set = '(GMT-03:00) Montevideo'; }
			if ( $gmt_time_zone == 'America/Miquelon' ) {
				$gmt_time_zone_set = '(GMT-03:00) Miquelon, St. Pierre'; }
			if ( $gmt_time_zone == 'America/Godthab' ) {
				$gmt_time_zone_set = '(GMT-03:00) Greenland'; }
			if ( $gmt_time_zone == 'America/Argentina/Buenos_Aires' ) {
				$gmt_time_zone_set = '(GMT-03:00) Buenos Aires'; }
			if ( $gmt_time_zone == 'America/Sao_Paulo' ) {
				$gmt_time_zone_set = '(GMT-03:00) Brasilia'; }
			if ( $gmt_time_zone == 'America/Noronha' ) {
				$gmt_time_zone_set = '(GMT-02:00) Mid-Atlantic'; }
			if ( $gmt_time_zone == 'Atlantic/Cape_Verde' ) {
				$gmt_time_zone_set = '(GMT-01:00) Cape Verde Is.'; }
			if ( $gmt_time_zone == 'Atlantic/Azores' ) {
				$gmt_time_zone_set = '(GMT-01:00) Azores'; }
			if ( $gmt_time_zone == 'Europe/Belfast' ) {
				$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Belfast'; }
			if ( $gmt_time_zone == 'Europe/Dublin' ) {
				$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Dublin'; }
			if ( $gmt_time_zone == 'Europe/Lisbon' ) {
				$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Lisbon'; }
			if ( $gmt_time_zone == 'Europe/London' ) {
				$gmt_time_zone_set = '(GMT) Greenwich Mean Time : London'; }
			if ( $gmt_time_zone == 'Africa/Abidjan' ) {
				$gmt_time_zone_set = '(GMT) Monrovia, Reykjavik'; }
			if ( $gmt_time_zone == 'Europe/Amsterdam' ) {
				$gmt_time_zone_set = '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'; }
			if ( $gmt_time_zone == 'Europe/Belgrade' ) {
				$gmt_time_zone_set = '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'; }
			if ( $gmt_time_zone == 'Europe/Brussels' ) {
				$gmt_time_zone_set = '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'; }
			if ( $gmt_time_zone == 'Africa/Algiers' ) {
				$gmt_time_zone_set = '(GMT+01:00) West Central Africa'; }
			if ( $gmt_time_zone == 'Africa/Windhoek' ) {
				$gmt_time_zone_set = '(GMT+01:00) Windhoek'; }
			if ( $gmt_time_zone == 'Asia/Beirut' ) {
				$gmt_time_zone_set = '(GMT+02:00) Beirut'; }
			if ( $gmt_time_zone == 'Africa/Cairo' ) {
				$gmt_time_zone_set = '(GMT+02:00) Cairo'; }
			if ( $gmt_time_zone == 'Asia/Gaza' ) {
				$gmt_time_zone_set = '(GMT+02:00) Gaza'; }
			if ( $gmt_time_zone == 'Africa/Blantyre' ) {
				$gmt_time_zone_set = '(GMT+02:00) Harare, Pretoria'; }
			if ( $gmt_time_zone == 'Asia/Jerusalem' ) {
				$gmt_time_zone_set = '(GMT+02:00) Jerusalem'; }
			if ( $gmt_time_zone == 'Europe/Minsk' ) {
				$gmt_time_zone_set = '(GMT+02:00) Minsk'; }
			if ( $gmt_time_zone == 'Asia/Damascus' ) {
				$gmt_time_zone_set = '(GMT+02:00) Syria'; }
			if ( $gmt_time_zone == 'Europe/Moscow' ) {
				$gmt_time_zone_set = '(GMT+03:00) Moscow, St. Petersburg, Volgograd'; }
			if ( $gmt_time_zone == 'Africa/Addis_Ababa' ) {
				$gmt_time_zone_set = '(GMT+03:00) Nairobi'; }
			if ( $gmt_time_zone == 'Asia/Tehran' ) {
				$gmt_time_zone_set = '(GMT+03:30) Tehran'; }
			if ( $gmt_time_zone == 'Asia/Dubai' ) {
				$gmt_time_zone_set = '(GMT+04:00) Abu Dhabi, Muscat'; }
			if ( $gmt_time_zone == 'Asia/Yerevan' ) {
				$gmt_time_zone_set = '(GMT+04:00) Yerevan'; }
			if ( $gmt_time_zone == 'Asia/Kabul' ) {
				$gmt_time_zone_set = '(GMT+04:30) Kabul'; }
			if ( $gmt_time_zone == 'Asia/Yekaterinburg' ) {
				$gmt_time_zone_set = '(GMT+05:00) Ekaterinburg'; }
			if ( $gmt_time_zone == 'Asia/Tashkent' ) {
				$gmt_time_zone_set = '(GMT+05:00) Tashkent'; }
			if ( $gmt_time_zone == 'Asia/Kolkata' ) {
				$gmt_time_zone_set = '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'; }
			if ( $gmt_time_zone == 'Asia/Katmandu' ) {
				$gmt_time_zone_set = '(GMT+05:45) Kathmandu'; }
			if ( $gmt_time_zone == 'Asia/Dhaka' ) {
				$gmt_time_zone_set = '(GMT+06:00) Astana, Dhaka'; }
			if ( $gmt_time_zone == 'Asia/Novosibirsk' ) {
				$gmt_time_zone_set = '(GMT+06:00) Novosibirsk'; }
			if ( $gmt_time_zone == 'Asia/Rangoon' ) {
				$gmt_time_zone_set = '(GMT+06:30) Yangon (Rangoon)'; }
			if ( $gmt_time_zone == 'Asia/Bangkok' ) {
				$gmt_time_zone_set = '(GMT+07:00) Bangkok, Hanoi, Jakarta'; }
			if ( $gmt_time_zone == 'Asia/Krasnoyarsk' ) {
				$gmt_time_zone_set = '(GMT+07:00) Krasnoyarsk'; }
			if ( $gmt_time_zone == 'Asia/Hong_Kong' ) {
				$gmt_time_zone_set = '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'; }
			if ( $gmt_time_zone == 'Asia/Irkutsk' ) {
				$gmt_time_zone_set = '(GMT+08:00) Irkutsk, Ulaan Bataar'; }
			if ( $gmt_time_zone == 'Australia/Perth' ) {
				$gmt_time_zone_set = '(GMT+08:00) Perth'; }
			if ( $gmt_time_zone == 'Australia/Eucla' ) {
				$gmt_time_zone_set = '(GMT+08:45) Eucla'; }
			if ( $gmt_time_zone == 'Asia/Tokyo' ) {
				$gmt_time_zone_set = '(GMT+09:00) Osaka, Sapporo, Tokyo'; }
			if ( $gmt_time_zone == 'Asia/Seoul' ) {
				$gmt_time_zone_set = '(GMT+09:00) Seoul'; }
			if ( $gmt_time_zone == 'Asia/Yakutsk' ) {
				$gmt_time_zone_set = '(GMT+09:00) Yakutsk'; }
			if ( $gmt_time_zone == 'Australia/Adelaide' ) {
				$gmt_time_zone_set = '(GMT+09:30) Adelaide'; }
			if ( $gmt_time_zone == 'Australia/Darwin' ) {
				$gmt_time_zone_set = '(GMT+09:30) Darwin'; }
			if ( $gmt_time_zone == 'Australia/Brisbane' ) {
				$gmt_time_zone_set = '(GMT+10:00) Brisbane'; }
			if ( $gmt_time_zone == 'Australia/Hobart' ) {
				$gmt_time_zone_set = '(GMT+10:00) Hobart'; }
			if ( $gmt_time_zone == 'Asia/Vladivostok' ) {
				$gmt_time_zone_set = '(GMT+10:00) Vladivostok'; }
			if ( $gmt_time_zone == 'Australia/Lord_Howe' ) {
				$gmt_time_zone_set = '(GMT+10:30) Lord Howe Island'; }
			if ( $gmt_time_zone == 'Etc/GMT-11' ) {
				$gmt_time_zone_set = '(GMT+11:00) Solomon Is., New Caledonia'; }
			if ( $gmt_time_zone == 'Asia/Magadan' ) {
				$gmt_time_zone_set = '(GMT+11:00) Magadan'; }
			if ( $gmt_time_zone == 'Pacific/Norfolk' ) {
				$gmt_time_zone_set = '(GMT+11:30) Norfolk Island'; }
			if ( $gmt_time_zone == 'Asia/Anadyr' ) {
				$gmt_time_zone_set = '(GMT+12:00) Anadyr, Kamchatka'; }
			if ( $gmt_time_zone == 'Pacific/Auckland' ) {
				$gmt_time_zone_set = '(GMT+12:00) Auckland, Wellington'; }
			if ( $gmt_time_zone == 'Etc/GMT-12' ) {
				$gmt_time_zone_set = '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'; }
			if ( $gmt_time_zone == 'Pacific/Chatham' ) {
				$gmt_time_zone_set = '(GMT+12:45) Chatham Islands'; }
			if ( $gmt_time_zone == 'Pacific/Tongatapu' ) {
				$gmt_time_zone_set = '(GMT+13:00) Nuku\'alofa'; }
			if ( $gmt_time_zone == 'Pacific/Kiritimati' ) {
				$gmt_time_zone_set = '(GMT+14:00) Kiritimati'; }

			if ( $admin_time == 'on' ) { // IF ADMIN TIME
				if ( $recurring_optin_times != '' || $recurring_optin_time != '' ) {
					foreach ( $recurring_optin_times as $recurring_optin_time ) {
						?>
						<option value="<?php echo $recurring_optin_time; ?>">
							<?php
							if ( $recurring_optin_time == '1' ) {
								echo '1:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '2' ) {
								echo '2:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '3' ) {
								echo '3:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '4' ) {
								echo '4:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '5' ) {
								echo '5:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '6' ) {
								echo '6:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '7' ) {
								echo '7:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '8' ) {
								echo '8:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '9' ) {
								echo '9:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '10' ) {
								echo '10:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '11' ) {
								echo '11:00 AM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '12' ) {
								echo '12:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '13' ) {
								echo '1:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '14' ) {
								echo '2:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '15' ) {
								echo '3:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '16' ) {
								echo '4:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '17' ) {
								echo '5:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '18' ) {
								echo '6:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '19' ) {
								echo '7:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '20' ) {
								echo '8:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '21' ) {
								echo '9:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '22' ) {
								echo '10:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '23' ) {
								echo '11:00 PM ' . $gmt_time_zone_set . ''; }
							if ( $recurring_optin_time == '24' ) {
								echo '12:00 AM ' . $gmt_time_zone_set . ''; }
							?>
						</option>
						<?php
					}
				}
			}
			?>
			</select>
			
		<?php } ?>
		
		<!-- ONETIME optin ON -->
		
		<?php if ( get_post_meta( $post_id, 'wpoptin_onetime_optin', true ) == 'on' ) { ?>
		
		<div id="onetimedate" style="text-align: left; margin-bottom: 15px;">
			<strong>optin Date:</strong> 
				<?php
				  $optinmonth = get_post_meta( $post_id, 'wpoptin_optin_month', true );
				if ( $optinmonth == '1' ) {
					echo 'January'; }
				if ( $optinmonth == '2' ) {
					echo 'February'; }
				if ( $optinmonth == '3' ) {
					echo 'March'; }
				if ( $optinmonth == '4' ) {
					echo 'April'; }
				if ( $optinmonth == '5' ) {
					echo 'May'; }
				if ( $optinmonth == '6' ) {
					echo 'June'; }
				if ( $optinmonth == '7' ) {
					echo 'July'; }
				if ( $optinmonth == '8' ) {
					echo 'August'; }
				if ( $optinmonth == '9' ) {
					echo 'September'; }
				if ( $optinmonth == '10' ) {
					echo 'October'; }
				if ( $optinmonth == '11' ) {
					echo 'November'; }
				if ( $optinmonth == '12' ) {
					echo 'December'; }
				?>
				<?php echo get_post_meta( $post_id, 'wpoptin_optin_day', true ); ?>, <?php echo get_post_meta( $post_id, 'wpoptin_optin_year', true ); ?>
			</br><strong>optin Time:</strong>
				<?php
				$admin_time    = get_post_meta( $post_id, 'wpoptin_admintime', true );
				$local_time    = get_post_meta( $post_id, 'wpoptin_localtime', true );
				$gmt_time_zone = get_post_meta( $post_id, 'wpoptin_optin_timezone', true );

				if ( $local_time == 'on' ) {
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '1' ) {
						echo '1:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '2' ) {
						echo '2:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '3' ) {
						echo '3:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '4' ) {
						echo '4:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '5' ) {
						echo '5:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '6' ) {
						echo '6:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '7' ) {
						echo '7:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '8' ) {
						echo '8:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '9' ) {
						echo '9:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '10' ) {
						echo '10:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '11' ) {
						echo '11:00 AM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '12' ) {
						echo '12:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '13' ) {
						echo '1:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '14' ) {
						echo '2:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '15' ) {
						echo '3:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '16' ) {
						echo '4:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '17' ) {
						echo '5:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '18' ) {
						echo '6:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '19' ) {
						echo '7:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '20' ) {
						echo '8:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '21' ) {
						echo '9:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '22' ) {
						echo '10:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '23' ) {
						echo '11:00 PM Local Time'; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '24' ) {
						echo '12:00 AM Local Time'; }
				}

				if ( $admin_time == 'on' ) {

					if ( $gmt_time_zone == 'Pacific/Midway' ) {
						$gmt_time_zone_set = '(GMT-11:00) Midway Island, Samoa'; }
					if ( $gmt_time_zone == 'America/Adak' ) {
						$gmt_time_zone_set = '(GMT-10:00) Hawaii-Aleutian'; }
					if ( $gmt_time_zone == 'Etc/GMT+10' ) {
						$gmt_time_zone_set = '(GMT-10:00) Hawaii'; }
					if ( $gmt_time_zone == 'Pacific/Marquesas' ) {
						$gmt_time_zone_set = '(GMT-09:30) Marquesas Islands'; }
					if ( $gmt_time_zone == 'Pacific/Gambier' ) {
						$gmt_time_zone_set = '(GMT-09:00) Gambier Islands'; }
					if ( $gmt_time_zone == 'America/Anchorage' ) {
						$gmt_time_zone_set = '(GMT-09:00) Alaska'; }
					if ( $gmt_time_zone == 'America/Ensenada' ) {
						$gmt_time_zone_set = '(GMT-08:00) Tijuana, Baja California'; }
					if ( $gmt_time_zone == 'Etc/GMT+8' ) {
						$gmt_time_zone_set = '(GMT-08:00) Pitcairn Islands'; }
					if ( $gmt_time_zone == 'America/Los_Angeles' ) {
						$gmt_time_zone_set = '(GMT-08:00) Pacific Time (US & Canada)'; }
					if ( $gmt_time_zone == 'America/Denver' ) {
						$gmt_time_zone_set = '(GMT-07:00) Mountain Time (US & Canada)'; }
					if ( $gmt_time_zone == 'America/Chihuahua' ) {
						$gmt_time_zone_set = '(GMT-07:00) Chihuahua, La Paz, Mazatlan'; }
					if ( $gmt_time_zone == 'America/Dawson_Creek' ) {
						$gmt_time_zone_set = '(GMT-07:00) Arizona'; }
					if ( $gmt_time_zone == 'America/Belize' ) {
						$gmt_time_zone_set = '(GMT-06:00) Saskatchewan, Central America'; }
					if ( $gmt_time_zone == 'America/Cancun' ) {
						$gmt_time_zone_set = '(GMT-06:00) Guadalajara, Mexico City, Monterrey'; }
					if ( $gmt_time_zone == 'Chile/EasterIsland' ) {
						$gmt_time_zone_set = '(GMT-06:00) Easter Island'; }
					if ( $gmt_time_zone == 'America/Chicago' ) {
						$gmt_time_zone_set = '(GMT-06:00) Central Time (US & Canada)'; }
					if ( $gmt_time_zone == 'America/New_York' ) {
						$gmt_time_zone_set = '(GMT-05:00) Eastern Time (US & Canada)'; }
					if ( $gmt_time_zone == 'America/Havana' ) {
						$gmt_time_zone_set = '(GMT-05:00) Cuba'; }
					if ( $gmt_time_zone == 'America/Bogota' ) {
						$gmt_time_zone_set = '(GMT-05:00) Bogota, Lima, Quito, Rio Branco'; }
					if ( $gmt_time_zone == 'America/Caracas' ) {
						$gmt_time_zone_set = '(GMT-04:30) Caracas'; }
					if ( $gmt_time_zone == 'America/Santiago' ) {
						$gmt_time_zone_set = '(GMT-04:00) Santiago'; }
					if ( $gmt_time_zone == 'America/La_Paz' ) {
						$gmt_time_zone_set = '(GMT-04:00) La Paz'; }
					if ( $gmt_time_zone == 'Atlantic/Stanley' ) {
						$gmt_time_zone_set = '(GMT-04:00) Faukland Islands'; }
					if ( $gmt_time_zone == 'America/Campo_Grande' ) {
						$gmt_time_zone_set = '(GMT-04:00) Brazil'; }
					if ( $gmt_time_zone == 'America/Goose_Bay' ) {
						$gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Goose Bay)'; }
					if ( $gmt_time_zone == 'America/Glace_Bay' ) {
						$gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Canada)'; }
					if ( $gmt_time_zone == 'America/St_Johns' ) {
						$gmt_time_zone_set = '(GMT-03:30) Newfoundland'; }
					if ( $gmt_time_zone == 'America/Araguaina' ) {
						$gmt_time_zone_set = '(GMT-03:00) UTC-3'; }
					if ( $gmt_time_zone == 'America/Montevideo' ) {
						$gmt_time_zone_set = '(GMT-03:00) Montevideo'; }
					if ( $gmt_time_zone == 'America/Miquelon' ) {
						$gmt_time_zone_set = '(GMT-03:00) Miquelon, St. Pierre'; }
					if ( $gmt_time_zone == 'America/Godthab' ) {
						$gmt_time_zone_set = '(GMT-03:00) Greenland'; }
					if ( $gmt_time_zone == 'America/Argentina/Buenos_Aires' ) {
						$gmt_time_zone_set = '(GMT-03:00) Buenos Aires'; }
					if ( $gmt_time_zone == 'America/Sao_Paulo' ) {
						$gmt_time_zone_set = '(GMT-03:00) Brasilia'; }
					if ( $gmt_time_zone == 'America/Noronha' ) {
						$gmt_time_zone_set = '(GMT-02:00) Mid-Atlantic'; }
					if ( $gmt_time_zone == 'Atlantic/Cape_Verde' ) {
						$gmt_time_zone_set = '(GMT-01:00) Cape Verde Is.'; }
					if ( $gmt_time_zone == 'Atlantic/Azores' ) {
						$gmt_time_zone_set = '(GMT-01:00) Azores'; }
					if ( $gmt_time_zone == 'Europe/Belfast' ) {
						$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Belfast'; }
					if ( $gmt_time_zone == 'Europe/Dublin' ) {
						$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Dublin'; }
					if ( $gmt_time_zone == 'Europe/Lisbon' ) {
						$gmt_time_zone_set = '(GMT) Greenwich Mean Time : Lisbon'; }
					if ( $gmt_time_zone == 'Europe/London' ) {
						$gmt_time_zone_set = '(GMT) Greenwich Mean Time : London'; }
					if ( $gmt_time_zone == 'Africa/Abidjan' ) {
						$gmt_time_zone_set = '(GMT) Monrovia, Reykjavik'; }
					if ( $gmt_time_zone == 'Europe/Amsterdam' ) {
						$gmt_time_zone_set = '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'; }
					if ( $gmt_time_zone == 'Europe/Belgrade' ) {
						$gmt_time_zone_set = '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'; }
					if ( $gmt_time_zone == 'Europe/Brussels' ) {
						$gmt_time_zone_set = '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'; }
					if ( $gmt_time_zone == 'Africa/Algiers' ) {
						$gmt_time_zone_set = '(GMT+01:00) West Central Africa'; }
					if ( $gmt_time_zone == 'Africa/Windhoek' ) {
						$gmt_time_zone_set = '(GMT+01:00) Windhoek'; }
					if ( $gmt_time_zone == 'Asia/Beirut' ) {
						$gmt_time_zone_set = '(GMT+02:00) Beirut'; }
					if ( $gmt_time_zone == 'Africa/Cairo' ) {
						$gmt_time_zone_set = '(GMT+02:00) Cairo'; }
					if ( $gmt_time_zone == 'Asia/Gaza' ) {
						$gmt_time_zone_set = '(GMT+02:00) Gaza'; }
					if ( $gmt_time_zone == 'Africa/Blantyre' ) {
						$gmt_time_zone_set = '(GMT+02:00) Harare, Pretoria'; }
					if ( $gmt_time_zone == 'Asia/Jerusalem' ) {
						$gmt_time_zone_set = '(GMT+02:00) Jerusalem'; }
					if ( $gmt_time_zone == 'Europe/Minsk' ) {
						$gmt_time_zone_set = '(GMT+02:00) Minsk'; }
					if ( $gmt_time_zone == 'Asia/Damascus' ) {
						$gmt_time_zone_set = '(GMT+02:00) Syria'; }
					if ( $gmt_time_zone == 'Europe/Moscow' ) {
						$gmt_time_zone_set = '(GMT+03:00) Moscow, St. Petersburg, Volgograd'; }
					if ( $gmt_time_zone == 'Africa/Addis_Ababa' ) {
						$gmt_time_zone_set = '(GMT+03:00) Nairobi'; }
					if ( $gmt_time_zone == 'Asia/Tehran' ) {
						$gmt_time_zone_set = '(GMT+03:30) Tehran'; }
					if ( $gmt_time_zone == 'Asia/Dubai' ) {
						$gmt_time_zone_set = '(GMT+04:00) Abu Dhabi, Muscat'; }
					if ( $gmt_time_zone == 'Asia/Yerevan' ) {
						$gmt_time_zone_set = '(GMT+04:00) Yerevan'; }
					if ( $gmt_time_zone == 'Asia/Kabul' ) {
						$gmt_time_zone_set = '(GMT+04:30) Kabul'; }
					if ( $gmt_time_zone == 'Asia/Yekaterinburg' ) {
						$gmt_time_zone_set = '(GMT+05:00) Ekaterinburg'; }
					if ( $gmt_time_zone == 'Asia/Tashkent' ) {
						$gmt_time_zone_set = '(GMT+05:00) Tashkent'; }
					if ( $gmt_time_zone == 'Asia/Kolkata' ) {
						$gmt_time_zone_set = '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'; }
					if ( $gmt_time_zone == 'Asia/Katmandu' ) {
						$gmt_time_zone_set = '(GMT+05:45) Kathmandu'; }
					if ( $gmt_time_zone == 'Asia/Dhaka' ) {
						$gmt_time_zone_set = '(GMT+06:00) Astana, Dhaka'; }
					if ( $gmt_time_zone == 'Asia/Novosibirsk' ) {
						$gmt_time_zone_set = '(GMT+06:00) Novosibirsk'; }
					if ( $gmt_time_zone == 'Asia/Rangoon' ) {
						$gmt_time_zone_set = '(GMT+06:30) Yangon (Rangoon)'; }
					if ( $gmt_time_zone == 'Asia/Bangkok' ) {
						$gmt_time_zone_set = '(GMT+07:00) Bangkok, Hanoi, Jakarta'; }
					if ( $gmt_time_zone == 'Asia/Krasnoyarsk' ) {
						$gmt_time_zone_set = '(GMT+07:00) Krasnoyarsk'; }
					if ( $gmt_time_zone == 'Asia/Hong_Kong' ) {
						$gmt_time_zone_set = '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'; }
					if ( $gmt_time_zone == 'Asia/Irkutsk' ) {
						$gmt_time_zone_set = '(GMT+08:00) Irkutsk, Ulaan Bataar'; }
					if ( $gmt_time_zone == 'Australia/Perth' ) {
						$gmt_time_zone_set = '(GMT+08:00) Perth'; }
					if ( $gmt_time_zone == 'Australia/Eucla' ) {
						$gmt_time_zone_set = '(GMT+08:45) Eucla'; }
					if ( $gmt_time_zone == 'Asia/Tokyo' ) {
						$gmt_time_zone_set = '(GMT+09:00) Osaka, Sapporo, Tokyo'; }
					if ( $gmt_time_zone == 'Asia/Seoul' ) {
						$gmt_time_zone_set = '(GMT+09:00) Seoul'; }
					if ( $gmt_time_zone == 'Asia/Yakutsk' ) {
						$gmt_time_zone_set = '(GMT+09:00) Yakutsk'; }
					if ( $gmt_time_zone == 'Australia/Adelaide' ) {
						$gmt_time_zone_set = '(GMT+09:30) Adelaide'; }
					if ( $gmt_time_zone == 'Australia/Darwin' ) {
						$gmt_time_zone_set = '(GMT+09:30) Darwin'; }
					if ( $gmt_time_zone == 'Australia/Brisbane' ) {
						$gmt_time_zone_set = '(GMT+10:00) Brisbane'; }
					if ( $gmt_time_zone == 'Australia/Hobart' ) {
						$gmt_time_zone_set = '(GMT+10:00) Hobart'; }
					if ( $gmt_time_zone == 'Asia/Vladivostok' ) {
						$gmt_time_zone_set = '(GMT+10:00) Vladivostok'; }
					if ( $gmt_time_zone == 'Australia/Lord_Howe' ) {
						$gmt_time_zone_set = '(GMT+10:30) Lord Howe Island'; }
					if ( $gmt_time_zone == 'Etc/GMT-11' ) {
						$gmt_time_zone_set = '(GMT+11:00) Solomon Is., New Caledonia'; }
					if ( $gmt_time_zone == 'Asia/Magadan' ) {
						$gmt_time_zone_set = '(GMT+11:00) Magadan'; }
					if ( $gmt_time_zone == 'Pacific/Norfolk' ) {
						$gmt_time_zone_set = '(GMT+11:30) Norfolk Island'; }
					if ( $gmt_time_zone == 'Asia/Anadyr' ) {
						$gmt_time_zone_set = '(GMT+12:00) Anadyr, Kamchatka'; }
					if ( $gmt_time_zone == 'Pacific/Auckland' ) {
						$gmt_time_zone_set = '(GMT+12:00) Auckland, Wellington'; }
					if ( $gmt_time_zone == 'Etc/GMT-12' ) {
						$gmt_time_zone_set = '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'; }
					if ( $gmt_time_zone == 'Pacific/Chatham' ) {
						$gmt_time_zone_set = '(GMT+12:45) Chatham Islands'; }
					if ( $gmt_time_zone == 'Pacific/Tongatapu' ) {
						$gmt_time_zone_set = '(GMT+13:00) Nuku\'alofa'; }
					if ( $gmt_time_zone == 'Pacific/Kiritimati' ) {
						$gmt_time_zone_set = '(GMT+14:00) Kiritimati'; }

					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '1' ) {
						echo '1:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '2' ) {
						echo '2:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '3' ) {
						echo '3:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '4' ) {
						echo '4:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '5' ) {
						echo '5:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '6' ) {
						echo '6:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '7' ) {
						echo '7:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '8' ) {
						echo '8:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '9' ) {
						echo '9:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '10' ) {
						echo '10:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '11' ) {
						echo '11:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '12' ) {
						echo '12:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '13' ) {
						echo '1:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '14' ) {
						echo '2:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '15' ) {
						echo '3:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '16' ) {
						echo '4:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '17' ) {
						echo '5:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '18' ) {
						echo '6:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '19' ) {
						echo '7:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '20' ) {
						echo '8:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '21' ) {
						echo '9:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '22' ) {
						echo '10:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '23' ) {
						echo '11:00 PM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
					if ( get_post_meta( $post_id, 'wpoptin_optin_hour', true ) == '24' ) {
						echo '12:00 AM';
						echo '</br><strong>Time Zone:</strong></br>';
						echo $gmt_time_zone_set; }
				}


				?>
		</div>
	
		<?php } ?>
			
		<!-- ****************************************************************** -->
		<!-- AUTORESPONDER -->
		<!-- ****************************************************************** -->

		<!-- ERASES NAME AND EMAIL INFO -->
		<script type="text/javascript"> function make_blank() { document.wpoptin.first_name.value =""; document.optin.email.value =""; } </script>
		
		<script type="text/javascript">
			function clickclear(thisfield, defaulttext) {
				if (thisfield.value == defaulttext) {
				thisfield.value = "";
				}
			}

			function clickrecall(thisfield, defaulttext) {
				if (thisfield.value == "") {
				thisfield.value = defaulttext;
				}
			}
		</script>
			
	<?php
	global $post;
	$post_id      = $post->ID;
	$formURL      = get_post_meta( $post_id, 'wpoptin_optin_form_url', true );
	$emailField   = get_post_meta( $post_id, 'wpoptin_optin_email_field', true );
	$nameField    = get_post_meta( $post_id, 'wpoptin_optin_name_field', true );
	$hiddenFields = get_post_meta( $post_id, 'wpoptin_optin_hidden_field', true );
	$buttontext   = get_post_meta( $post_id, 'wpoptin_button_text', true );
	?>
	 

				<?php
				if ( $moderntheme == 'on' ) {
					echo '<div class="optintext"><b>3.)</b> Where Do We Send The Invite?</div>';
				}
				?>
			<input type="text" class="name" name="first_name" id="first_name" value="Enter Your First Name" onblur="copyName();clickrecall(this,'Enter Your First Name');" onFocus="javascript:if(this.value=='Enter Your First Name') this.value='';"/>  
			<input type="text" class="email" name="email" id="email" value="Type In Your Email Address" onblur="copyEmail();clickrecall(this,'Type In Your Email Address');" onFocus="javascript:if(this.value=='Type In Your Email Address') this.value='';"/>
			<input type="submit" name="go" id="go" class="optinpagebutton" value="<?php echo $buttontext; ?>"/>
			</form>
	
  
			<form id="form2" name="form2" method="post" action="<?php echo $formURL; ?>">
				<input type="hidden" id="autoname" name="<?php echo $nameField; ?>" value='First Name'/>
				<input type="hidden" id="autoemail" name="<?php echo $emailField; ?>" value='Email'/>
				<?php echo $hiddenFields; ?>
			</form>

	</div><!-- END optin SIDEBAR -->
	</div>
</div><!-- END WRAPPER OPTIN -->
<?php } ?>


<?php
global $post;
$post_id   = $post->ID;
$values    = $_GET['optin'];
$optintime = $_GET['time'];
$optindate = $_GET['date'];
$type      = $_GET['type'];
$emailto   = $_GET['email'];
$person    = $_GET['first_name'];


// SETS LOCAL TIME ZONE
if ( $values == 'processing1' ) {
	if ( ! isset( $_SESSION['timezone'] ) ) {
		if ( ! isset( $_REQUEST['offset'] ) ) {
			?>
			<script>
			var d = new Date()
			var offset= -d.getTimezoneOffset()/60;
			location.href = "<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin=processing2&type=<?php echo $_GET['type']; ?>&date=<?php echo $_GET['date']; ?>&time=<?php echo $_GET['time']; ?>&email=<?php echo $emailto; ?>&person=<?php echo $person; ?>&offset="+offset;

			</script>
			<?php
		} else {
			$zonelist             = array(
				'Kwajalein'                      => -12.00,
				'Pacific/Midway'                 => -11.00,
				'Pacific/Honolulu'               => -10.00,
				'America/Anchorage'              => -9.00,
				'America/Los_Angeles'            => -8.00,
				'America/Denver'                 => -7.00,
				'America/Tegucigalpa'            => -6.00,
				'America/New_York'               => -5.00,
				'America/Caracas'                => -4.30,
				'America/Halifax'                => -4.00,
				'America/St_Johns'               => -3.30,
				'America/Argentina/Buenos_Aires' => -3.00,
				'America/Sao_Paulo'              => -3.00,
				'Atlantic/South_Georgia'         => -2.00,
				'Atlantic/Azores'                => -1.00,
				'Europe/Dublin'                  => 0,
				'Europe/Belgrade'                => 1.00,
				'Europe/Minsk'                   => 2.00,
				'Asia/Kuwait'                    => 3.00,
				'Asia/Tehran'                    => 3.30,
				'Asia/Muscat'                    => 4.00,
				'Asia/Yekaterinburg'             => 5.00,
				'Asia/Kolkata'                   => 5.30,
				'Asia/Katmandu'                  => 5.45,
				'Asia/Dhaka'                     => 6.00,
				'Asia/Rangoon'                   => 6.30,
				'Asia/Krasnoyarsk'               => 7.00,
				'Asia/Brunei'                    => 8.00,
				'Asia/Seoul'                     => 9.00,
				'Australia/Darwin'               => 9.30,
				'Australia/Canberra'             => 10.00,
				'Asia/Magadan'                   => 11.00,
				'Pacific/Fiji'                   => 12.00,
				'Pacific/Tongatapu'              => 13.00,
			);
			$index                = array_keys( $zonelist, $_REQUEST['offset'] );
			$_SESSION['timezone'] = $index[0];
		}
	}
	date_default_timezone_set( $_SESSION['timezone'] );
}
?>



<?php
global $post;
$post_id   = $post->ID;
$values    = $_GET['optin'];
$optintime = $_GET['time'];
$optindate = $_GET['date'];
$type      = $_GET['type'];
$emailto   = $_GET['email'];

$from_name  = get_post_meta( $post_id, 'wpoptin_from_name', true );
$from_email = get_post_meta( $post_id, 'wpoptin_from_email', true );

$subject_one = get_post_meta( $post_id, 'wpoptin_email_one_subject', true );
$message_one = get_post_meta( $post_id, 'wpoptin_email_one_message', true );

$subject_1before = get_post_meta( $post_id, 'wpoptin_email_1hour_subject', true );
$message_1before = get_post_meta( $post_id, 'wpoptin_email_1hour_message', true );

$subject_24before = get_post_meta( $post_id, 'wpoptin_email_24hour_subject', true );
$message_24before = get_post_meta( $post_id, 'wpoptin_email_24hour_message', true );

$subject_replay = get_post_meta( $post_id, 'wpoptin_email_replay_subject', true );
$message_replay = get_post_meta( $post_id, 'wpoptin_email_replay_message', true );

$linkperm     = get_permalink( $_REQUEST['posted'] );
$thankyoutype = '?optin=wait&type=';
$anddate      = '&date=';
$andtime      = '&time=';

$optinlink  = $linkperm . $thankyoutype . $type . $anddate . $optindate . $andtime . $optintime;
$insertdate = date( 'l F jS, Y', strtotime( $optindate ) );

if ( $optintime == '1' ) {
	$newoptintime = '1:00 AM Local Time'; }
if ( $optintime == '2' ) {
	$newoptintime = '2:00 AM Local Time'; }
if ( $optintime == '3' ) {
	$newoptintime = '3:00 AM Local Time'; }
if ( $optintime == '4' ) {
	$newoptintime = '4:00 AM Local Time'; }
if ( $optintime == '5' ) {
	$newoptintime = '5:00 AM Local Time'; }
if ( $optintime == '6' ) {
	$newoptintime = '6:00 AM Local Time'; }
if ( $optintime == '7' ) {
	$newoptintime = '7:00 AM Local Time'; }
if ( $optintime == '8' ) {
	$newoptintime = '8:00 AM Local Time'; }
if ( $optintime == '9' ) {
	$newoptintime = '9:00 AM Local Time'; }
if ( $optintime == '10' ) {
	$newoptintime = '10:00 AM Local Time'; }
if ( $optintime == '11' ) {
	$newoptintime = '11:00 AM Local Time'; }
if ( $optintime == '12' ) {
	$newoptintime = '12:00 PM Local Time'; }
if ( $optintime == '13' ) {
	$newoptintime = '1:00 PM Local Time'; }
if ( $optintime == '14' ) {
	$newoptintime = '2:00 PM Local Time'; }
if ( $optintime == '15' ) {
	$newoptintime = '3:00 PM Local Time'; }
if ( $optintime == '16' ) {
	$newoptintime = '4:00 PM Local Time'; }
if ( $optintime == '17' ) {
	$newoptintime = '5:00 PM Local Time'; }
if ( $optintime == '18' ) {
	$newoptintime = '6:00 PM Local Time'; }
if ( $optintime == '19' ) {
	$newoptintime = '7:00 PM Local Time'; }
if ( $optintime == '20' ) {
	$newoptintime = '8:00 PM Local Time'; }
if ( $optintime == '21' ) {
	$newoptintime = '9:00 PM Local Time'; }
if ( $optintime == '22' ) {
	$newoptintime = '10:00 PM Local Time'; }
if ( $optintime == '23' ) {
	$newoptintime = '11:00 PM Local Time'; }
if ( $optintime == '24' ) {
	$newoptintime = '12:00 AM Local Time'; }

// Message One
$new_message_one   = str_replace( '%optinLINK%', $optinlink, $message_one );
$new_message_two   = str_replace( '%optinDATE%', $insertdate, $new_message_one );
$new_message_final = str_replace( '%optinTIME%', $newoptintime, $new_message_two );

// Message 1 Hour
$new_message_1before       = str_replace( '%optinLINK%', $optinlink, $message_1before );
$new_message_1before_two   = str_replace( '%optinDATE%', $insertdate, $new_message_1before );
$new_message_1before_final = str_replace( '%optinTIME%', $newoptintime, $new_message_1before_two );

// Message 24
$new_message_24before       = str_replace( '%optinLINK%', $optinlink, $message_24before );
$new_message_24before_two   = str_replace( '%optinDATE%', $insertdate, $new_message_24before );
$new_message_24before_final = str_replace( '%optinTIME%', $newoptintime, $new_message_24before_two );

// Message Replay
$new_message_replay       = str_replace( '%optinLINK%', $optinlink, $message_replay );
$new_message_replay_two   = str_replace( '%optinDATE%', $insertdate, $new_message_replay );
$new_message_replay_final = str_replace( '%optinTIME%', $newoptintime, $new_message_replay_two );


if ( $values == 'processing2' ) {
	?>

	<?php

			$zonelist             = array(
				'Kwajalein'                      => -12.00,
				'Pacific/Midway'                 => -11.00,
				'Pacific/Honolulu'               => -10.00,
				'America/Anchorage'              => -9.00,
				'America/Los_Angeles'            => -8.00,
				'America/Denver'                 => -7.00,
				'America/Tegucigalpa'            => -6.00,
				'America/New_York'               => -5.00,
				'America/Caracas'                => -4.30,
				'America/Halifax'                => -4.00,
				'America/St_Johns'               => -3.30,
				'America/Argentina/Buenos_Aires' => -3.00,
				'America/Sao_Paulo'              => -3.00,
				'Atlantic/South_Georgia'         => -2.00,
				'Atlantic/Azores'                => -1.00,
				'Europe/Dublin'                  => 0,
				'Europe/Belgrade'                => 1.00,
				'Europe/Minsk'                   => 2.00,
				'Asia/Kuwait'                    => 3.00,
				'Asia/Tehran'                    => 3.30,
				'Asia/Muscat'                    => 4.00,
				'Asia/Yekaterinburg'             => 5.00,
				'Asia/Kolkata'                   => 5.30,
				'Asia/Katmandu'                  => 5.45,
				'Asia/Dhaka'                     => 6.00,
				'Asia/Rangoon'                   => 6.30,
				'Asia/Krasnoyarsk'               => 7.00,
				'Asia/Brunei'                    => 8.00,
				'Asia/Seoul'                     => 9.00,
				'Australia/Darwin'               => 9.30,
				'Australia/Canberra'             => 10.00,
				'Asia/Magadan'                   => 11.00,
				'Pacific/Fiji'                   => 12.00,
				'Pacific/Tongatapu'              => 13.00,
			);
			$index                = array_keys( $zonelist, $_REQUEST['offset'] );
			$_SESSION['timezone'] = $index[0];

			date_default_timezone_set( $_SESSION['timezone'] );

			?>

	<?php
	// Initial Email
	wp_mail(
		'' . $emailto . '',
		'' . $subject_one . '',
		'' . $new_message_final . '',
		array(
			'From: ' . $from_name . ' <' . $from_email . '>',
		)
	);

	// 1 Hour Before
	wp_delayed_mail(
		strtotime( '-1 hours', strtotime( "$optindate $optintime:00" ) ),
		'' . $emailto . '',
		'' . $subject_1before . '',
		'' . $new_message_1before_final . '',
		array(
			'From: ' . $from_name . ' <' . $from_email . '>',
		)
	);

	// 24 Hours Before
	wp_delayed_mail(
		strtotime( '-1 days', strtotime( "$optindate $optintime:00" ) ),
		'' . $emailto . '',
		'' . $subject_24before . '',
		'' . $new_message_24before_final . '',
		array(
			'From: ' . $from_name . ' <' . $from_email . '>',
		)
	);

	// 24 Hours After
	wp_delayed_mail(
		strtotime( '+1 days', strtotime( "$optindate $optintime:00" ) ),
		'' . $emailto . '',
		'' . $subject_replay . '',
		'' . $new_message_replay_final . '',
		array(
			'From: ' . $from_name . ' <' . $from_email . '>',
		)
	);
	?>
	
	<!-- ****************************************************************** -->
	<!-- optin IN BETWEEN PAGE -->
	<!-- ****************************************************************** -->
	
	<script type="text/javascript">
		<!--
		setTimeout("window.location = '<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin=thankyou&type=<?php echo $_GET['type']; ?>&date=<?php echo $_GET['date']; ?>&time=<?php echo $_GET['time']; ?>&email=<?php echo $_GET['email']; ?>&person=<?php echo $_GET['person']; ?>';",5000);
		//-->
	</script>
	
	<div id="optin_wrapper"><!-- START optin WRAPPER BETWEEN PAGE -->
	<div id="waiting">
		<div id="loading2"style="font-size: 24px; font-weight: bold; text-align: center;"><img src="
		<?php
		echo $wp_plugin_url;
		echo '/wp-optin/images/generator.gif';
		?>
		" width="100" height="100"/> </div>
		<div id="wait" style="font-size: 24px; font-weight: bold; text-align: center;">Please Wait While We Process Your Registration!</div>
	</div>
	
	</div><!-- END WRAPPER THANK YOU PAGE -->
<?php } ?>


<!-- NEW THANK YOU PAGE -->

<?php
global $post;
$post_id    = $post->ID;
$values     = $_GET['optin'];
$optintime  = $_GET['time'];
$optindate  = $_GET['date'];
$optinemail = $_GET['email'];
$wbdates    = strtotime( $optindate );
$type       = $_GET['type'];
$person     = $_GET['person'];
if ( $values == 'thankyou' ) {
	?>
	<!-- Countdown dashboard start -->
	<!-- ****************************************************************** -->
	<!-- optin THANK YOU PAGE -->
	<!-- ****************************************************************** -->
	<div id="optin_wrapper"><!-- START WRAPPER THANK YOU -->
	
		<div id="optin_thankyou_side">
		<h2>Your optin Details:</h2>
			<div id="optin_details_side">
			
				<strong>Name: </strong><?php echo $person; ?></br>
				<strong>Email: </strong><?php echo $optinemail; ?></br>
				<strong>optin Date: </strong><?php echo date( 'F jS, Y', strtotime( $optindate ) ); ?></br>

				<strong>optin Time: </strong>
				<?php
				if ( $optintime == '1' ) {
					echo '1:00 AM'; }
				if ( $optintime == '2' ) {
					echo '2:00 AM'; }
				if ( $optintime == '3' ) {
					echo '3:00 AM'; }
				if ( $optintime == '4' ) {
					echo '4:00 AM'; }
				if ( $optintime == '5' ) {
					echo '5:00 AM'; }
				if ( $optintime == '6' ) {
					echo '6:00 AM'; }
				if ( $optintime == '7' ) {
					echo '7:00 AM'; }
				if ( $optintime == '8' ) {
					echo '8:00 AM'; }
				if ( $optintime == '9' ) {
					echo '9:00 AM'; }
				if ( $optintime == '10' ) {
					echo '10:00 AM'; }
				if ( $optintime == '11' ) {
					echo '11:00 AM'; }
				if ( $optintime == '12' ) {
					echo '12:00 PM'; }
				if ( $optintime == '13' ) {
					echo '1:00 PM'; }
				if ( $optintime == '14' ) {
					echo '2:00 PM'; }
				if ( $optintime == '15' ) {
					echo '3:00 PM'; }
				if ( $optintime == '16' ) {
					echo '4:00 PM'; }
				if ( $optintime == '17' ) {
					echo '5:00 PM'; }
				if ( $optintime == '18' ) {
					echo '6:00 PM'; }
				if ( $optintime == '19' ) {
					echo '7:00 PM'; }
				if ( $optintime == '20' ) {
					echo '8:00 PM'; }
				if ( $optintime == '21' ) {
					echo '9:00 PM'; }
				if ( $optintime == '22' ) {
					echo '10:00 PM'; }
				if ( $optintime == '23' ) {
					echo '11:00 PM'; }
				if ( $optintime == '24' ) {
					echo '12:00 AM'; }
				?>
					</br>
			
			</div>
			
			<div id="optin_step">
				<img src="
				<?php
				echo $wp_plugin_url;
				echo '/wp-optin/images/printit.png';
				?>
				" width="280" height="48"/></br>
				<p>Click the link below to print this page and be sure to set it in a place where you can see it to remind you of this optin. </p>
				<center><strong><A class="printbutton" HREF="javascript:window.print()">Click Here to Print This Page</A></strong></center>
			</div>
			
			<div id="optin_step">
				<img src="
				<?php
				echo $wp_plugin_url;
				echo '/wp-optin/images/shareit.png';
				?>
				" width="280" height="48"/></br>
				<p>Share this optin with your friends. Click the buttons below to share it now.</p>
				
				<div id="media_wrapper" style="width: 90%; margin: 0 auto;">
				
				<!-- FACEBOOK CODE -->
				<div id= "optin_media_button">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=186068724768141";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-href="<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin" data-send="false" data-layout="box_count" data-show-faces="true"></div>
				</div>
				
				<!-- TWITTER CODE -->
				<div id= "optin_media_button" style="margin-right: 18px;">
					<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-url="<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin" data-text="I just signed up for an amazing optin. Here's the link: <?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				
				<!-- GOOGLE+ CODE -->
				<div id= "optin_media_button">
					<g:plusone size="tall" href="<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin"></g:plusone>
					<!-- Place this render call where appropriate -->
					<script type="text/javascript">
					  (function() {
						var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
					  })();
					</script>
				</div>
				
				<div id= "optin_media_button">
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
				 lang: en_US
				</script>
				<script type="IN/Share" data-url="<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin" data-counter="top"></script>
				</div>
				
				</div>
			</div>
		
		</div>
		
		<div id="optin_content">
			<div id="optin_video">
					
				<!-- ****************************************************************** -->
				<!-- Thank You Page Text -->
				<!-- ****************************************************************** -->
				<h2>Congratulations You're Registered!</h2>
				
				<?php
				global $post;
				$post_id = $post->ID;
				$values  = $_GET['optin'];

				$thankyou_video_url    = get_post_meta( $post_id, 'wpoptin_thankyou_page_video_url', true );
				$thankyou_video_width  = get_post_meta( $post_id, 'wpoptin_thankyou_page_video_width', true );
				$thankyou_video_height = get_post_meta( $post_id, 'wpoptin_thankyou_page_video_height', true );

				?>
				
				<!-- ****************************************************************** -->
				<!-- THANK YOU PAGE VIDEO -->
				<!-- ****************************************************************** -->
				
		<?php

		if ( strstr( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPod' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) ) {
			echo "<video width='";
			echo $thankyou_video_width;
			echo "' height='";
			echo $thankyou_video_height;
			echo "' controls='controls' autoplay='autoplay'><source src='";
			echo $thankyou_video_url;
			echo "'/>";
		} else {
			echo '<center><a href="#" style="border: 1px solid #ccc;background-color:black;display:block;width:';
			echo $thankyou_video_width;
			echo 'px;height:';
			echo $thankyou_video_height;
			echo 'px;z-index:1;" id="player"></a>' . "\n";
			echo '	<script type="text/javascript">' . "\n";
			echo '	$f("player", "';
			echo $wp_plugin_url;
			echo '/wp-optin/player/flowplayer-3.2.7.swf';
			echo '", { ' . "\n";
			echo '		onBegin: function () {' . "\n";
			echo '			this.setVolume(100);' . "\n";
			echo '		},' . "\n";
			echo '		clip: { ' . "\n";
			echo '			subTitle: \'optin\', ' . "\n";
			echo '			autoPlay:true, url:\'';
			echo $thankyou_video_url;
			echo '\',' . "\n";
			echo '			baseUrl:\'\' },' . "\n";
			echo '		onStart:function() { ' . "\n";
			echo '			this.seek(0); },' . "\n";
			echo '		onBeforeFinish: function () { ' . "\n";
			echo '			this.getPlugin("play").hide();' . "\n";
			echo '			},' . "\n";
			echo '		canvas: { ' . "\n";
			echo '			backgroundColor: "#000" },' . "\n";
			echo '		plugins: { ' . "\n";
			echo '			controls: { ' . "\n";
			echo '				all: false, ' . "\n";
			echo '				opacity: 0} ' . "\n";
			echo '	} }).ipad(); ' . "\n";
			echo '	</script>' . "\n";
			echo '	</center>' . "\n";
		}
		?>
			</div>
			<div style="clear: both;"></div>
			
			<div id="optin_link">
			<h2>Here's Your optin Link:</h2>
			<?php
				$shorty = get_permalink( $_REQUEST['posted'] ) . '?optin=wait&type=' . $_GET['type'] . '&date=' . $_GET['date'] . '&time=' . $_GET['time'];
				$turl   = getTinyUrl( $shorty );
				echo '<a href="' . $turl . '">' . $turl . '</a>'
			?>
			</div>
		</div>
			<div style="clear: both;"></div>
			
			

	</div><!-- END WRAPPER THANK YOU PAGE -->
	<div style="clear: both;"></div>
<?php } ?>

<!-- END NEW THANK YOU PAGE -->


<?php
global $post;
$post_id   = $post->ID;
$values    = $_GET['optin'];
$optintime = $_GET['time'];
$optindate = $_GET['date'];
$wbdates   = strtotime( $optindate );
$type      = $_GET['type'];
if ( $values == 'wait' ) {
	?>
	<!-- Countdown dashboard start -->
	<!-- ****************************************************************** -->
	<!-- optin THANK YOU PAGE -->
	<!-- ****************************************************************** -->
	<div id="optin_wrapper"><!-- START WRAPPER THANK YOU -->
		<div id="countdown_dashboard" style="width: 520px; margin: 0 auto; padding-right: 20px;">

			<div class="dash days_dash">
				<span class="dash_title">days</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash hours_dash">
				<span class="dash_title">hours</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash minutes_dash">
				<span class="dash_title">minutes</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash seconds_dash">
				<span class="dash_title">seconds</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

		</div>

		<?php if ( $type == 'o' ) { ?>

			<script language="javascript" type="text/javascript">
				jQuery(document).ready(function() {
					$('#countdown_dashboard').countDown({
						targetDate: {
							'day': 		<?php echo get_post_meta( $post_id, 'wpoptin_optin_day', true ); ?>,
							'month': 	<?php echo get_post_meta( $post_id, 'wpoptin_optin_month', true ); ?>,
							'year': 	<?php echo get_post_meta( $post_id, 'wpoptin_optin_year', true ); ?>,
							'hour': 	<?php echo get_post_meta( $post_id, 'wpoptin_optin_hour', true ); ?>,
							'min': 		0,
							'sec': 		0,

						},
					// onComplete function
					onComplete: function() { window.location.href='<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin=play' }
					});
				});
			</script>

		<?php } ?>

		<?php if ( $type == 'r' ) { ?>

			<script language="javascript" type="text/javascript">
				jQuery(document).ready(function() {
					$('#countdown_dashboard').countDown({
						targetDate: {
							'day': 		<?php echo date( 'd', $wbdates ); ?>,
							'month': 	<?php echo date( 'm', $wbdates ); ?>,
							'year': 	<?php echo date( 'Y', $wbdates ); ?>,
							'hour': 	<?php echo $optintime; ?>,
							'min': 		0,
							'sec': 		0
						},
						// onComplete function
						onComplete: function() { window.location.href='<?php echo get_permalink( $_REQUEST['posted'] ); ?>?optin=play' }
					});
				});
			</script>

		<?php } ?>
		
		<!-- ****************************************************************** -->
		<!-- Thank You Page Text -->
		<!-- ****************************************************************** -->

		<?php
		global $post;
		$post_id = $post->ID;
			echo '<div style="clear:both">';
			echo '<div id="thankyou">';
			echo '<center><h2>The optin Will Start When The Countdown Reaches Zero</h2></center>';
			echo '</div>';
		?>
		

</div><!-- END WRAPPER THANK YOU PAGE -->
<?php } ?>

<?php
global $post;
$post_id   = $post->ID;
$values    = $_GET['optin'];
$optintime = $_GET['time'];
$optindate = $_GET['date'];

$optin_video_url    = get_post_meta( $post_id, 'wpoptin_optin_video_url', true );
$optin_video_width  = get_post_meta( $post_id, 'wpoptin_optin_video_width', true );
$optin_video_height = get_post_meta( $post_id, 'wpoptin_optin_video_height', true );

if ( $values == 'play' || $values == 'replay' ) {
	?>
	<div id="optin_wrapper"><!-- START WRAPPER PLAY REPLAY -->
		<!-- ****************************************************************** -->
		<!-- optin PAGE -->
		<!-- ****************************************************************** -->
	<?php
	$minutes = get_post_meta( $post_id, 'wpoptin_minutes', true );
	$seconds = get_post_meta( $post_id, 'wpoptin_seconds', true );
	$delay   = ( $minutes * 60 ) + $seconds;

	$playtimezone = get_post_meta( $post_id, 'wpoptin_optin_timezone', true );
	$nowdate      = date( 'Y-m-d G:i:s', strtotime( 'now' ) );
	$originaltz   = DateTime::createFromFormat( 'Y-m-d G:i:s', "$nowdate", new DateTimeZone( 'UTC' ) );
	$newtz        = $originaltz;
	$newtz->setTimeZone( new DateTimeZone( "$playtimezone" ) );
	$playtimenow  = $newtz->format( 'Y-m-d G:i:s' );
	$currenttime  = strtotime( $playtimenow );
	$optinstart   = strtotime( "$optindate $optintime:00:00" );
	$delayseconds = $currenttime - $optinstart;



	if ( strstr( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPod' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) ) {
		echo "<video width='";
		echo $optin_video_width;
		echo "' height='";
		echo $optin_video_height;
		echo "' controls='controls' autoplay='autoplay'><source src='";
		echo $optin_video_url;
		echo "'/>";
	} else {
		echo '<center>' . "\n";
		echo '<a href="#" style="border: 1px solid #ccc;background-color:black;display:block;width:';
		echo $optin_video_width;
		echo 'px;height:';
		echo $optin_video_height;
		echo 'px;z-index:1;" id="player"></a></center>' . "\n";
		echo '<script type="text/javascript">' . "\n";
		echo '	var info = document.getElementById("info");' . "\n";
		echo '	$f("player", "';
		echo $wp_plugin_url;
		echo '/wp-optin/player/flowplayer-3.2.7.swf';
		echo '", { ' . "\n";
		echo '		onBegin: function () {' . "\n";
		echo '			this.setVolume(100);' . "\n";
		echo '		},' . "\n";
		echo '		clip: {' . "\n";
		echo '			subTitle: \'optin\',' . "\n";
		echo '			autoPlay:true,' . "\n";
		echo '			url:\'';
		echo $optin_video_url;
		echo '\',' . "\n";
		echo '			baseUrl:\'\',' . "\n";
		echo '			onBeforePause : function(){ ' . "\n";
		echo '				return false; },' . "\n";
		echo '			onBeforeFinish: function () { ' . "\n";
		echo '				this.getPlugin("play").hide();' . "\n";
		echo '				},' . "\n";
		echo '			onCuepoint: [[';
		echo $delay;
		echo '000], function() { ' . "\n";
		echo '				document.getElementById(\'info\').style.display=\'block\';' . "\n";
		echo '			}]' . "\n";
		echo '		},' . "\n";
		// echo '       onStart:function() {' . "\n";
		// echo '           this.seek(';
		// echo $delayseconds;
		// echo '); },' . "\n";
		echo '		canvas: { ' . "\n";
		echo '			backgroundColor: "#000" },' . "\n";
		echo '		plugins: { ' . "\n";
		echo '			controls: { ' . "\n";
		echo '				all: false, ' . "\n";
		echo '				opacity: 0}' . "\n";
		echo '			}' . "\n";
		echo '	}).ipad();' . "\n";
		echo '</script>' . "\n";
	}

	echo '</center>' . "\n";



	?>

<div id="info" style="display: none; margin-top: 25px;">
	<?php echo get_post_meta( $post_id, 'wpoptin_call_to_action', true ); ?>
</div>
	</div><!-- END WRAPPER PLAY REPLAY -->
<div style="clear:both"></div>	

<?php } ?>

<?php
global $post;
$post_id     = $post->ID;
$yourtheme   = get_post_meta( $post_id, 'wpoptin_theme_yours', true );
$basictheme  = get_post_meta( $post_id, 'wpoptin_theme_basic', true );
$affiliateid = get_post_meta( $post_id, 'wpoptin_affiliateid', true );


if ( $yourtheme == 'on' ) {
	get_footer();
}
if ( $basictheme == 'on' ) {
	echo '</div></body></html>';
}
if ( $moderntheme == 'on' ) {
	echo '</div></body></html>';
}
?>
